﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TaskManagerWebApi.Models;
using System.Web.Routing;
using Newtonsoft.Json;

namespace TaskManagerWebApi.Controllers.v1
{
    public class AccountController : ApiController
    {
        [HttpGet]
        [Route("api/v1/test")]
        public string Test()
        {
            return "ok";
        }


        /// <summary>
        /// bejelentkezést ellenörző művelet, tényleges beléptetés nincs mögötte. Bárki hívhatja.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/login")]
        public HttpResponseMessage Login(LoginModel login)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person user = db.Person.Where(a => a.UserName == login.Username && a.Password == login.Password && a.IsActive).FirstOrDefault();
                if (user != null)
                {
                    var res = new HttpResponseMessage();
                    if (user.IsAdmin) { res.Content = new StringContent("true", System.Text.Encoding.Unicode); } else { res.Content = new StringContent("false", System.Text.Encoding.Unicode); }
                    res.StatusCode = HttpStatusCode.OK;
                    return res;
                }
                else
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.Unauthorized;
                    return res;
                }
            }

        }

        /// <summary>
        /// Új felhasználó létrehozása, csak admin hívhatja.
        /// </summary>
        /// <param name="newUser"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/newuser")]
        public HttpResponseMessage NewUser(NewUserModel newUser)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                creator = db.Person.Where(a => a.UserName == newUser.Username && a.Password == newUser.Password && a.IsActive).FirstOrDefault();
                if (creator == null)
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.Unauthorized;
                    return res;
                }
                if (creator.IsAdmin)
                {

                    if (db.Person.FirstOrDefault(a => a.UserName == newUser.NewUserUsername && a.IsActive) != null) {
                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.Conflict;
                        res.Content = new StringContent("fogalat felhasználóbév", System.Text.Encoding.Unicode);
                        return res;
                    }
                    if (db.Person.FirstOrDefault(a => a.Email == newUser.NewUserEmail && a.IsActive) != null)
                    {
                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.Conflict;
                        res.Content = new StringContent("fogalat e-mail", System.Text.Encoding.Unicode);
                        return res;
                    }


                    try
                    {
                        Person p = new Person();
                        p.UserName = newUser.NewUserUsername;
                        p.Name = newUser.NewUserName;
                        p.Password = newUser.NewUserPassword;
                        p.Email = newUser.NewUserEmail;
                        p.IsAdmin = newUser.NewUserAdmin;
                        p.Creator = creator;
                        p.IsActive = true;
                        db.Person.Add(p);
                        db.SaveChanges();

                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.Created;
                        return res;
                    }
                    catch (Exception ex)
                    {
                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.Unauthorized;
                        return res;
                    }



                    
                }
                else
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.Unauthorized;
                    return res;
                }
            }

        }

        /// <summary>
        /// Az összes felhasználót visszadó metódus, bárki hívhatja.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/getallusers")]
        public HttpResponseMessage GetAllUsers(LoginModel login)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                creator = db.Person.Where(a => a.UserName == login.Username && a.Password == login.Password && a.IsActive).FirstOrDefault();
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }



                    try
                    {
                        var r = new HttpResponseMessage();
                        r.Content = new StringContent(JsonConvert.SerializeObject(db.Person.Where(a => a.IsActive).ToList()));



                        r.StatusCode = HttpStatusCode.OK;
                        return r;
                    }
                    catch (Exception ex)
                    {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                    return res;
                }
            }

        }
        /// <summary>
        /// Az adminokat visszaadó metódus, bárki hívhatja.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/getalladmins")]
        public HttpResponseMessage GetAllAdmins(LoginModel login)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                creator = db.Person.Where(a => a.UserName == login.Username && a.Password == login.Password && a.IsActive).FirstOrDefault();
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }



                try
                {
                    var r = new HttpResponseMessage();
                    r.Content = new StringContent(JsonConvert.SerializeObject(db.Person.Where(a => a.IsActive && a.IsAdmin).ToList()));



                    r.StatusCode = HttpStatusCode.OK;
                    return r;
                }
                catch (Exception ex)
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                    return res;
                }
            }

        }
        /// <summary>
        /// A csoport vezetőket visszaadó metódus, bárki hívhatja.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/getallleaders")]
        public HttpResponseMessage GetAllLeaders(LoginModel login)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                creator = db.Person.Where(a => a.UserName == login.Username && a.Password == login.Password && a.IsActive).FirstOrDefault();
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }



                try
                {
                    var r = new HttpResponseMessage();
                    var users = db.Person.Where(a => a.IsActive).ToList();
                    users = users.Where(a => a.LeadGroups.Count > 0).ToList();
                    r.Content = new StringContent(JsonConvert.SerializeObject(users));



                    r.StatusCode = HttpStatusCode.OK;
                    return r;
                }
                catch (Exception ex)
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                    return res;
                }
            }

        }
        /// <summary>
        /// A képességeket visszaadó metódus, bárki hívhatja.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/getallskills")]
        public HttpResponseMessage GetSkills(LoginModel login)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person user = new Person();
                user = db.Person.Where(a => a.UserName == login.Username && a.Password == login.Password && a.IsActive).FirstOrDefault();
                if (user == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }



                try
                {
                    var r = new HttpResponseMessage();
                    var content = db.Skill.ToList();

                   
                    r.Content = new StringContent(JsonConvert.SerializeObject(content));



                    r.StatusCode = HttpStatusCode.OK;
                    return r;
                }
                catch (Exception ex)
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                    return res;
                }
            }

        }
        /// <summary>
        /// A saját képességeket visszaadó metódus, bárki hívhatja
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/getownskills")]
        public HttpResponseMessage GetOwnSkills(LoginModel login)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person user = new Person();
                user = db.Person.Where(a => a.UserName == login.Username && a.Password == login.Password && a.IsActive).FirstOrDefault();
                if (user == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }



                try
                {
                    var r = new HttpResponseMessage();
                    var content = user.Skills.ToList();


                    r.Content = new StringContent(JsonConvert.SerializeObject(content));



                    r.StatusCode = HttpStatusCode.OK;
                    return r;
                }
                catch (Exception ex)
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                    return res;
                }
            }

        }
        /// <summary>
        /// Képességet felhasználóhoz adó metódus, csak admi hívhatja.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/addskilltouser")]
        public HttpResponseMessage AddSkillToUser(SubjectObjectModel login)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person user = new Person();
                user = db.Person.Where(a => a.UserName == login.Username && a.Password == login.Password && a.IsActive && a.IsAdmin).FirstOrDefault();
                if (user == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }



                try
                {
                    Person subject = db.Person.Where(a => a.Id == login.Subject).FirstOrDefault();
                    var skill = db.Skill.FirstOrDefault(a => a.Id == login.Object);
                    if (subject == null || skill== null) { var res = new HttpResponseMessage(); res.StatusCode = HttpStatusCode.NotFound;  return res;
                    }


                    var r = new HttpResponseMessage();
                   

                    if (!user.Skills.Contains(skill))
                    { user.Skills.Add(skill); db.SaveChanges(); }


                    r.StatusCode = HttpStatusCode.OK;
                    return r;
                }
                catch (Exception ex)
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                    return res;
                }
            }

        }
        /// <summary>
        /// Képességet felhasználótól eltávolító metódus, csak admin hívhatja.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/removeskillfromuser")]
        public HttpResponseMessage RemoveSkillFromUser(SubjectObjectModel login)
        {
            
            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person user = new Person();
                user = db.Person.Where(a => a.UserName == login.Username && a.Password == login.Password && a.IsActive && a.IsAdmin).FirstOrDefault();
                if (user == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }



                try
                {
                    Person subject = db.Person.Where(a => a.Id == login.Subject).FirstOrDefault();
                    var skill = db.Skill.FirstOrDefault(a => a.Id == login.Object);
                    if (subject == null || skill == null)
                    {
                        var res = new HttpResponseMessage(); res.StatusCode = HttpStatusCode.NotFound; return res;
                    }


                    var r = new HttpResponseMessage();


                    if (user.Skills.Contains(skill))
                    { user.Skills.Remove(skill); db.SaveChanges(); }


                    r.StatusCode = HttpStatusCode.OK;
                    return r;
                }
                catch (Exception ex)
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                    return res;
                }
            }

        }
        /// <summary>
        /// Felhasználót csoporthoz adó metódus, csak admin hívhatja.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/addusertogroup")]
        public HttpResponseMessage AddUserToGroup(SubjectObjectModel login)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person user = new Person();
                user = db.Person.Where(a => a.UserName == login.Username && a.Password == login.Password && a.IsActive && a.IsAdmin).FirstOrDefault();
                if (user == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }



                try
                {
                    Person subject = db.Person.Where(a => a.Id == login.Subject).FirstOrDefault();
                    var group = db.Group.FirstOrDefault(a => a.Id == login.Object);
                    if (subject == null || group == null)
                    {
                        var res = new HttpResponseMessage(); res.StatusCode = HttpStatusCode.NotFound; return res;
                    }


                    var r = new HttpResponseMessage();


                    if (!group.Members.Contains(subject))
                    { group.Members.Add(subject); db.SaveChanges(); }


                    r.StatusCode = HttpStatusCode.OK;
                    return r;
                }
                catch (Exception ex)
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                    return res;
                }
            }

        }
        /// <summary>
        /// Felhasználót csoportból eltávolító metódus, csak admin hívhatja.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/removeuserfromgroup")]
        public HttpResponseMessage RemoveUserFromGroup(SubjectObjectModel login)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person user = new Person();
                user = db.Person.Where(a => a.UserName == login.Username && a.Password == login.Password && a.IsActive && a.IsAdmin).FirstOrDefault();
                if (user == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }



                try
                {
                    Person subject = db.Person.Where(a => a.Id == login.Subject).FirstOrDefault();
                    var group = db.Group.FirstOrDefault(a => a.Id == login.Object);
                    if (subject == null || group == null)
                    {
                        var res = new HttpResponseMessage(); res.StatusCode = HttpStatusCode.NotFound; return res;
                    }


                    var r = new HttpResponseMessage();


                    if (group.Members.Contains(subject))
                    { group.Members.Remove(subject); db.SaveChanges(); }



                    r.StatusCode = HttpStatusCode.OK;
                    return r;
                }
                catch (Exception ex)
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                    return res;
                }
            }

        }
        /// <summary>
        /// Csoportvezető csoporthoz adása, csak admin hívhatja.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/addleadertogroup")]
        public HttpResponseMessage AddLeaderToGroup(SubjectObjectModel login)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person user = new Person();
                user = db.Person.Where(a => a.UserName == login.Username && a.Password == login.Password && a.IsActive && a.IsAdmin).FirstOrDefault();
                if (user == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }



                try
                {
                    Person subject = db.Person.Where(a => a.Id == login.Subject).FirstOrDefault();
                    var group = db.Group.FirstOrDefault(a => a.Id == login.Object);
                    if (subject == null || group == null)
                    {
                        var res = new HttpResponseMessage(); res.StatusCode = HttpStatusCode.NotFound; return res;
                    }


                    var r = new HttpResponseMessage();


                    if (!group.Leaders.Contains(subject))
                    { group.Leaders.Add(subject); db.SaveChanges(); }


                    r.StatusCode = HttpStatusCode.OK;
                    return r;
                }
                catch (Exception ex)
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                    return res;
                }
            }

        }
        /// <summary>
        /// Csoportvezető csoportból eltávolítása, csak admin hívhatja.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/removeleaderfromgroup")]
        public HttpResponseMessage RemoveLeaderFromGroup(SubjectObjectModel login)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person user = new Person();
                user = db.Person.Where(a => a.UserName == login.Username && a.Password == login.Password && a.IsActive && a.IsAdmin).FirstOrDefault();
                if (user == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }



                try
                {
                    Person subject = db.Person.Where(a => a.Id == login.Subject).FirstOrDefault();
                    var group = db.Group.FirstOrDefault(a => a.Id == login.Object);
                    if (subject == null || group == null)
                    {
                        var res = new HttpResponseMessage(); res.StatusCode = HttpStatusCode.NotFound; return res;
                    }


                    var r = new HttpResponseMessage();


                    if (group.Leaders.Contains(subject))
                    { group.Leaders.Remove(subject); db.SaveChanges(); }



                    r.StatusCode = HttpStatusCode.OK;
                    return r;
                }
                catch (Exception ex)
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                    return res;
                }
            }

        }
        /// <summary>
        /// Új képesség felvétele, csak admin hívatja.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/newskill")]
        public HttpResponseMessage NewSkill(NewSkillModel login)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person user = new Person();
                user = db.Person.Where(a => a.UserName == login.Username && a.Password == login.Password && a.IsActive && a.IsAdmin).FirstOrDefault();
                if (user == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }

                if (!user.IsAdmin) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized }; }

                try
                {
                    Skill skill = new Skill() { Name = login.Name };

                    db.Skill.Add(skill);
                    db.SaveChanges();

                    var r = new HttpResponseMessage();
                    r.StatusCode = HttpStatusCode.Created;
                    return r;
                }
                catch (Exception ex)
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                    return res;
                }
            }

        }
        /// <summary>
        /// Képsség letörlése, csak admin hívhatja.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/deleteskill")]
        public HttpResponseMessage DeleteSkill(IdModel login)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person user = new Person();
                user = db.Person.Where(a => a.UserName == login.Username && a.Password == login.Password && a.IsActive && a.IsAdmin).FirstOrDefault();
                if (user == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }

                if (!user.IsAdmin) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized }; }

                try
                {
                    
                    var skill = db.Skill.FirstOrDefault(a => a.Id == login.Id);
                    if (skill == null)
                    {
                        var res = new HttpResponseMessage(); res.StatusCode = HttpStatusCode.NotFound; return res;
                    }


                    var r = new HttpResponseMessage();

                    //db.Person.Select(a => a.Skills.Remove(skill));

                    skill.Persons.Clear();
                    db.SaveChanges();
                    db.Skill.Remove(skill);
                    db.SaveChanges();

                    r.StatusCode = HttpStatusCode.OK;
                    return r;
                }
                catch (Exception ex)
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                    return res;
                }
            }

        }
        /// <summary>
        /// Felhasználó id alapján való lekérdezése, bárki hívhatja.
        /// </summary>
        /// <param name="getuser"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/getuserbyid")]
        public HttpResponseMessage GetUserById(IdModel getuser)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                Person person = new Person();
                creator = db.Person.Where(a => a.UserName == getuser.Username && a.Password == getuser.Password && a.IsActive).FirstOrDefault();
                person = db.Person.FirstOrDefault(a => a.Id == getuser.Id);
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (person == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen felhasználó id: {getuser.Id}", System.Text.Encoding.Unicode) }; }



                    try
                    {
                        var r = new HttpResponseMessage();
                        r.Content = new StringContent(JsonConvert.SerializeObject(person));
                        r.StatusCode = HttpStatusCode.OK;
                        return r;
                    }
                    catch (Exception ex)
                    {
                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.InternalServerError;
                        res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                        return res;
                    }

            }

        }
        /// <summary>
        /// Felhasználó letörlése, csak admin hívhatja.
        /// </summary>
        /// <param name="getuser"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/DeleteUser")]
        public HttpResponseMessage DeleteUser(IdModel getuser)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                Person person = new Person();
                creator = db.Person.Where(a => a.UserName == getuser.Username && a.Password == getuser.Password && a.IsActive && a.IsAdmin).FirstOrDefault();
                person = db.Person.FirstOrDefault(a => a.Id == getuser.Id);
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (person == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen felhasználó id: {getuser.Id}", System.Text.Encoding.Unicode) }; }



                try
                {
                    person.Tasks.Clear();
                    person.Groups.Clear();
                    person.SubTasks.Clear();
                    person.LeadGroups.Clear();
                    person.IsActive = false;
                    db.SaveChanges();
                    var r = new HttpResponseMessage();
                    r.StatusCode = HttpStatusCode.OK;
                    return r;
                }
                catch (Exception ex)
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                    return res;
                }

            }

        }

        /// <summary>
        /// Felhasználó lekérdezése felhasználónév alapján bárki hívhatja.
        /// </summary>
        /// <param name="getuser"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/GetUserByUsername")]
        public HttpResponseMessage GetUserByUsername(NameModel getuser)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                Person person = new Person();
                creator = db.Person.Where(a => a.UserName == getuser.Username && a.Password == getuser.Password && a.IsActive).FirstOrDefault();
                person = db.Person.FirstOrDefault(a => a.UserName == getuser.Name);
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (person == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen felhasználó username: {getuser.Username}", System.Text.Encoding.Unicode) }; }



                try
                {
                    var r = new HttpResponseMessage();
                    r.Content = new StringContent(JsonConvert.SerializeObject(person));
                    r.StatusCode = HttpStatusCode.OK;
                    return r;
                }
                catch (Exception ex)
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                    return res;
                }

            }

        }
        /// <summary>
        /// Felhasználóhoz tartozó csoportok lehívása, bárki hívhatja.
        /// </summary>
        /// <param name="getuser"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/GetGroupsByUser")]
        public HttpResponseMessage GetGroupsByUser(IdModel getuser)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                Person person = new Person();
                creator = db.Person.Where(a => a.UserName == getuser.Username && a.Password == getuser.Password && a.IsActive).FirstOrDefault();
                person = db.Person.FirstOrDefault(a => a.Id == getuser.Id);
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (person == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen felhasználó username: {getuser.Username}", System.Text.Encoding.Unicode) }; }



                try
                {
                    var r = new HttpResponseMessage();
                    r.Content = new StringContent(JsonConvert.SerializeObject(person.Groups.Where(a=>a.IsActive)));
                    r.StatusCode = HttpStatusCode.OK;
                    return r;
                }
                catch (Exception ex)
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                    return res;
                }

            }

        }
        /// <summary>
        /// Felhasználóhoz tartozó egyedi feladatok lehívása, bárki hívhatja.
        /// </summary>
        /// <param name="getuser"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/GetTasksByUser")]
        public HttpResponseMessage GetTasksByUser(IdModel getuser)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                Person person = new Person();
                creator = db.Person.Where(a => a.UserName == getuser.Username && a.Password == getuser.Password && a.IsActive).FirstOrDefault();
                person = db.Person.FirstOrDefault(a => a.Id == getuser.Id);
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (person == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen felhasználó username: {getuser.Username}", System.Text.Encoding.Unicode) }; }



                try
                {
                    var r = new HttpResponseMessage();
                    r.Content = new StringContent(JsonConvert.SerializeObject(person.Tasks.Where(a => a.IsActive)));
                    r.StatusCode = HttpStatusCode.OK;
                    return r;
                }
                catch (Exception ex)
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                    return res;
                }

            }

        }
        /// <summary>
        /// Felhasználóhoz tartozó részfeladatok lehívása, bárki hívhatja.
        /// </summary>
        /// <param name="getuser"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/GetSubtasksByUser")]
        public HttpResponseMessage GetSubtasksByUser(IdModel getuser)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                Person person = new Person();
                creator = db.Person.Where(a => a.UserName == getuser.Username && a.Password == getuser.Password && a.IsActive).FirstOrDefault();
                person = db.Person.FirstOrDefault(a => a.Id == getuser.Id);
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (person == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen felhasználó username: {getuser.Username}", System.Text.Encoding.Unicode) }; }



                try
                {
                    var r = new HttpResponseMessage();
                    r.Content = new StringContent(JsonConvert.SerializeObject(person.SubTasks.Where(a => a.IsActive)));
                    r.StatusCode = HttpStatusCode.OK;
                    return r;
                }
                catch (Exception ex)
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                    return res;
                }

            }

        }

    }
}

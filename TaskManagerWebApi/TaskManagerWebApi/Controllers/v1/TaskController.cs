﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TaskManagerWebApi.Models;
using Newtonsoft.Json;

namespace TaskManagerWebApi.Controllers.v1
{
    public class TaskController : ApiController
    {
        /// <summary>
        /// Részfeladat törlése, csak admin vagy adott csoport vezetője hívhatja.
        /// </summary>
        /// <param name="getsubtask"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/DeleteSubTask")]
        public HttpResponseMessage DeleteSubTask(IdModel getsubtask)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                SubTask subtask = new SubTask();
                creator = db.Person.Where(a => a.UserName == getsubtask.Username && a.Password == getsubtask.Password && a.IsActive ).FirstOrDefault();
                subtask = db.SubTask.FirstOrDefault(a => a.Id == getsubtask.Id);
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (subtask == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen részfeladat id: {getsubtask.Id}", System.Text.Encoding.Unicode) }; }
                if (!creator.IsAdmin && !subtask.ParentTask.Group.Leaders.Contains(creator)) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent($"Ennek a részfeladat tölrléséhez nincs jogosultsága id: {getsubtask.Id}", System.Text.Encoding.Unicode) }; }
      


                try
                {
                    subtask.IsActive = false;
                    subtask.Persons.Clear();
                    subtask.ParentTask.SubTask.Remove(subtask);

                    db.SaveChanges();


                    db.SaveChanges();
                    var r = new HttpResponseMessage();
                    r.StatusCode = HttpStatusCode.OK;
                    return r;
                }
                catch (Exception ex)
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                    return res;
                }

            }

        }
        /// <summary>
        /// Feladat törlése, csak admin hívhatja.
        /// </summary>
        /// <param name="gettask"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/DeleteTask")]
        public HttpResponseMessage DeleteGroup(IdModel gettask)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                Task task = new Task();
                creator = db.Person.Where(a => a.UserName == gettask.Username && a.Password == gettask.Password && a.IsActive).FirstOrDefault();
                task = db.Task.FirstOrDefault(a => a.Id == gettask.Id);
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (task == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen feladat id: {gettask.Id}", System.Text.Encoding.Unicode) }; }
                if (!creator.IsAdmin) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent($"Ennek a részfeladat tölrléséhez nincs jogosultsága id: {task.Id}", System.Text.Encoding.Unicode) }; }



                try
                {
                    task.IsActive = false;
                    task.SubTask.Clear();
                    task.Group = null;
                    

                    db.SaveChanges();
                    var r = new HttpResponseMessage();
                    r.StatusCode = HttpStatusCode.OK;
                    return r;
                }
                catch (Exception ex)
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                    return res;
                }

            }

        }
        /// <summary>
        /// Új feladat létrehozása, cask admin hívhatja.
        /// </summary>
        /// <param name="newTask"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/newtask")]
        public HttpResponseMessage NewTask(NewTaskModel newTask)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                creator = db.Person.Where(a => a.UserName == newTask.Username && a.Password == newTask.Password && a.IsActive).FirstOrDefault();
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (creator.IsAdmin)
                {


                    try
                    {
                        var result = new HttpResponseMessage();
                        result.StatusCode = HttpStatusCode.Created;


                        Task t = new Task();
                        t.Creator = creator;
                        t.Deadline = newTask.Deadline;
                        t.IsActive = true;
                        t.CreationDate = DateTime.Today;
                        t.Progress = 0;
                        t.Comment = newTask.Comment;
                        t.StartDate = newTask.StartDate;
                        t.Title = newTask.Title;

                        db.Task.Add(t);
                        db.SaveChanges();
                        return result;
                    }
                    catch (Exception ex)
                    {
                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.Unauthorized;
                        res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                        return res;
                    }




                }
                else
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    return res;
                }
            }

        }
        /// <summary>
        /// Adott feladat részfeladatának lehívácsa, admin csoportvezetők, csoporttagok hívhatják.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/GetSubtasksByTask")]
        public HttpResponseMessage GetAllTasks(IdModel login)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                Task task = new Task();
                creator = db.Person.Where(a => a.UserName == login.Username && a.Password == login.Password && a.IsActive).FirstOrDefault();
                task = db.Task.Where(a => a.IsActive && a.Id == login.Id).FirstOrDefault();
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (task == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen feladat {login.Id}", System.Text.Encoding.Unicode) }; }

                if (creator.IsAdmin || task.Group.Members.Contains(creator) || task.Group.Leaders.Contains(creator))
                {


                    try
                    {
                        var r = new HttpResponseMessage();
                        var tasks =task.SubTask.Where(a => a.IsActive).ToList();
                        r.Content = new StringContent(JsonConvert.SerializeObject(tasks));


                        r.StatusCode = HttpStatusCode.OK;
                        return r;
                    }
                    catch (Exception ex)
                    {
                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.InternalServerError;
                        res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                        return res;
                    }




                }
                else
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.Unauthorized;
                    return res;
                }
            }

        }
        /// <summary>
        /// Összes feladat lehívácsa, csak admin hívhatja.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/getalltasks")]
        public HttpResponseMessage GetAllTasks(LoginModel login)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                creator = db.Person.Where(a => a.UserName == login.Username && a.Password == login.Password && a.IsActive).FirstOrDefault();
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (creator.IsAdmin)
                {


                    try
                    {
                        var r = new HttpResponseMessage();
                        var tasks = db.Task.Where(a=>a.IsActive).ToList();
                        r.Content = new StringContent(JsonConvert.SerializeObject(tasks));


                        r.StatusCode = HttpStatusCode.OK;
                        return r;
                    }
                    catch (Exception ex)
                    {
                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.InternalServerError;
                        res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                        return res;
                    }




                }
                else
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.Unauthorized;              
                    return res;
                }
            }

        }
        [HttpPost]
        [Route("api/v1/GetFreeTasks")]
        public HttpResponseMessage GetFreeTasks(LoginModel login)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                creator = db.Person.Where(a => a.UserName == login.Username && a.Password == login.Password && a.IsActive).FirstOrDefault();
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (creator.IsAdmin)
                {


                    try
                    {
                        var r = new HttpResponseMessage();
                        var tasks = db.Task.Where(a => a.IsActive && a.Group==null).ToList();
                        r.Content = new StringContent(JsonConvert.SerializeObject(tasks));


                        r.StatusCode = HttpStatusCode.OK;
                        return r;
                    }
                    catch (Exception ex)
                    {
                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.InternalServerError;
                        res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                        return res;
                    }




                }
                else
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.Unauthorized;
                    return res;
                }
            }

        }
        /// <summary>
        /// Csoport feladatainak hívácsa, admin csoportvezetők, tagok hívhatják.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/GetTasksByGroup")]
        public HttpResponseMessage GetTasksByGroup(IdModel login)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                Group group = new Group();
                creator = db.Person.Where(a => a.UserName == login.Username && a.Password == login.Password && a.IsActive).FirstOrDefault();
                group = db.Group.FirstOrDefault(a => a.Id == login.Id && a.IsActive);
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (group == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nem található ilyen csoport id: {login.Id}", System.Text.Encoding.Unicode) }; }

                if (creator.IsAdmin || group.Members.Contains(creator) || group.Leaders.Contains(creator))
                {


                    try
                    {
                        var r = new HttpResponseMessage();
                        var tasks = group.Tasks.Where(a => a.IsActive).ToList();
                        r.Content = new StringContent(JsonConvert.SerializeObject(tasks));


                        r.StatusCode = HttpStatusCode.OK;
                        return r;
                    }
                    catch (Exception ex)
                    {
                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.InternalServerError;
                        res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                        return res;
                    }




                }
                else
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.Unauthorized;
                    return res;
                }
            }

        }
        /// <summary>
        /// Feladat csoporthoz rendelése, csak admin hívhatja.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/assigntasktogroup")]
        public HttpResponseMessage AssignTaskToGroup(SubjectObjectModel login)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person user = new Person();
                user = db.Person.Where(a => a.UserName == login.Username && a.Password == login.Password && a.IsActive && a.IsAdmin).FirstOrDefault();
                if (user == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }



                try
                {
                    Task task = db.Task.Where(a => a.Id == login.Object).FirstOrDefault();
                    var group = db.Group.FirstOrDefault(a => a.Id == login.Subject);
                    if (task == null || group == null)
                    {
                        var res = new HttpResponseMessage(); res.StatusCode = HttpStatusCode.NotFound; return res;
                    }


                    var r = new HttpResponseMessage();


                    if (task.Group != group)
                    { task.Group = group; db.SaveChanges(); }


                    r.StatusCode = HttpStatusCode.OK;
                    return r;
                }
                catch (Exception ex)
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                    return res;
                }
            }

        }
        /// <summary>
        /// Feladat törlése csoportból, csak admin hívhatja.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/removetaskfromgroup")]
        public HttpResponseMessage RemoveTaskFromGroup(SubjectObjectModel login)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person user = new Person();
                user = db.Person.Where(a => a.UserName == login.Username && a.Password == login.Password && a.IsActive && a.IsAdmin).FirstOrDefault();
                if (user == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }



                try
                {
                    Task subject = db.Task.Where(a => a.Id == login.Subject).FirstOrDefault();
                    var group = db.Group.FirstOrDefault(a => a.Id == login.Object);
                    if (subject == null || group == null)
                    {
                        var res = new HttpResponseMessage(); res.StatusCode = HttpStatusCode.NotFound; return res;
                    }


                    var r = new HttpResponseMessage();


                    if (subject.Group == group)
                    { subject.Group = null; db.SaveChanges(); }



                    r.StatusCode = HttpStatusCode.OK;
                    return r;
                }
                catch (Exception ex)
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                    return res;
                }
            }

        }
        /// <summary>
        /// Új részfeladat. admin és csoportvezetők hívhatják.
        /// </summary>
        /// <param name="newTask"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/newsubtask")]
        public HttpResponseMessage NewSubTask(NewSubTaskModel newTask)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                Task task = new Task();
                creator = db.Person.Where(a => a.UserName == newTask.Username && a.Password == newTask.Password && a.IsActive).FirstOrDefault();
                task = db.Task.FirstOrDefault(a => a.Id == newTask.ParentId);
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (task == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen feladat id: {newTask.ParentId}", System.Text.Encoding.Unicode) }; }
                if (creator.IsAdmin || task.Group.Leaders.Contains(creator))
                {


                    try
                    {
                        SubTask t = new SubTask();
                        t.IsActive = true;
                        t.Progress = 0;
                        t.Title = newTask.Title;
                        t.ParentTask = task;

                        db.SubTask.Add(t);
                        db.SaveChanges();

                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.Created;
                        return res;
                    }
                    catch (Exception ex)
                    {
                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.InternalServerError;
                        res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                        return res;
                    }




                }
                else
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.Unauthorized;
                    return res;
                }
            }

        }
        /// <summary>
        /// feladat lekérdezése id alapján. admin, csoportvezetők, csoporttagok hívhatják.
        /// </summary>
        /// <param name="newTask"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/gettask")]
        public HttpResponseMessage GetTask(IdModel newTask)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                Task task = new Task();
                creator = db.Person.Where(a => a.UserName == newTask.Username && a.Password == newTask.Password && a.IsActive).FirstOrDefault();
                task = db.Task.FirstOrDefault(a => a.Id == newTask.Id);
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (task == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen feladat id: {newTask.Id}", System.Text.Encoding.Unicode) }; }
                if (creator.IsAdmin || task.Group.Leaders.Contains(creator) || task.Group.Members.Contains(creator))
                {


                    try
                    {
                        var r = new HttpResponseMessage();
                        r.Content = new StringContent(JsonConvert.SerializeObject(task));
                        r.StatusCode = HttpStatusCode.OK;
                        return r;
                    }
                    catch (Exception ex)
                    {
                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.InternalServerError;
                        res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                        return res;
                    }
                }
                else
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.Unauthorized;
                    return res;
                }
            }

        }
        /// <summary>
        /// részfeladat lekérdezése id alapján. admin, csoportvezetők, csoporttagok hívhatják.
        /// </summary>
        /// <param name="newTask"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/getsubtask")]
        public HttpResponseMessage GetSubTask(IdModel newTask)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                SubTask task = new SubTask();
                creator = db.Person.Where(a => a.UserName == newTask.Username && a.Password == newTask.Password && a.IsActive).FirstOrDefault();
                task = db.SubTask.FirstOrDefault(a => a.Id == newTask.Id);
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (task == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen részfeladat id: {newTask.Id}", System.Text.Encoding.Unicode) }; }
                if (creator.IsAdmin || task.ParentTask.Group.Leaders.Contains(creator) || task.ParentTask.Group.Members.Contains(creator))
                {


                    try
                    {
                        var r = new HttpResponseMessage();
                        r.Content = new StringContent(JsonConvert.SerializeObject(task));
                        r.StatusCode = HttpStatusCode.OK;
                        return r;
                    }
                    catch (Exception ex)
                    {
                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.InternalServerError;
                        res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                        return res;
                    }
                }
                else
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.Unauthorized;
                    return res;
                }
            }

        }
        [HttpPost]
        [Route("api/v1/getsubtaskpersons")]
        public HttpResponseMessage GetSubTaskPersons(IdModel newTask)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                SubTask task = new SubTask();
                creator = db.Person.Where(a => a.UserName == newTask.Username && a.Password == newTask.Password && a.IsActive).FirstOrDefault();
                task = db.SubTask.FirstOrDefault(a => a.Id == newTask.Id);
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (task == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen részfeladat id: {newTask.Id}", System.Text.Encoding.Unicode) }; }
                if (creator.IsAdmin || task.ParentTask.Group.Leaders.Contains(creator) || task.ParentTask.Group.Members.Contains(creator))
                {


                    try
                    {
                        var r = new HttpResponseMessage();
                        r.Content = new StringContent(JsonConvert.SerializeObject(task.Persons));
                        r.StatusCode = HttpStatusCode.OK;
                        return r;
                    }
                    catch (Exception ex)
                    {
                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.InternalServerError;
                        res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                        return res;
                    }
                }
                else
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.Unauthorized;
                    return res;
                }
            }

        }

        [HttpPost]
        [Route("api/v1/GetOwnSubTasksByTask")]
        public HttpResponseMessage GetOwnSubTasksByTask(IdModel newTask)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                Task task = new Task();
                creator = db.Person.Where(a => a.UserName == newTask.Username && a.Password == newTask.Password && a.IsActive).FirstOrDefault();
                task = db.Task.FirstOrDefault(a => a.Id == newTask.Id);
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (task == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen részfeladat id: {newTask.Id}", System.Text.Encoding.Unicode) }; }
                if (creator.IsAdmin || task.Group.Leaders.Contains(creator) || task.Group.Members.Contains(creator))
                {


                    try
                    {
                        var r = new HttpResponseMessage();
                        r.Content = new StringContent(JsonConvert.SerializeObject(task.SubTask.Where(a=>a.Persons.Contains(creator))));
                        r.StatusCode = HttpStatusCode.OK;
                        return r;
                    }
                    catch (Exception ex)
                    {
                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.InternalServerError;
                        res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                        return res;
                    }
                }
                else
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.Unauthorized;
                    return res;
                }
            }

        }
        [HttpPost]
        [Route("api/v1/GetGroupByTask")]
        public HttpResponseMessage GetGroupByTask(IdModel newTask)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                Task task = new Task();
                creator = db.Person.Where(a => a.UserName == newTask.Username && a.Password == newTask.Password && a.IsActive).FirstOrDefault();
                task = db.Task.FirstOrDefault(a => a.Id == newTask.Id);
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (task == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen feladat id: {newTask.Id}", System.Text.Encoding.Unicode) }; }
                if (creator.IsAdmin || task.Group.Leaders.Contains(creator) || task.Group.Members.Contains(creator))
                {


                    try
                    {
                        var r = new HttpResponseMessage();
                        r.Content = new StringContent(JsonConvert.SerializeObject(task.Group));
                        r.StatusCode = HttpStatusCode.OK;
                        return r;
                    }
                    catch (Exception ex)
                    {
                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.InternalServerError;
                        res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                        return res;
                    }
                }
                else
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.Unauthorized;
                    return res;
                }
            }

        }
        /// <summary>
        /// összes részfeladat lekérdezése, csak admin hívhatja.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/getallsubtasks")]
        public HttpResponseMessage GetAllSubStasks(LoginModel login)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                creator = db.Person.Where(a => a.UserName == login.Username && a.Password == login.Password && a.IsActive).FirstOrDefault();
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (creator.IsAdmin)
                {


                    try
                    {
                        var r = new HttpResponseMessage();
                        r.Content = new StringContent(JsonConvert.SerializeObject(db.SubTask.Where(a => a.IsActive).ToList()));


                        r.StatusCode = HttpStatusCode.OK;
                        return r;
                    }
                    catch (Exception ex)
                    {
                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.InternalServerError;
                        res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                        return res;
                    }




                }
                else
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.Unauthorized;
                    return res;
                }
            }

        }
        /// <summary>
        /// feladat megjegyzés módosítása, admin és csoportvezetők hívhatják.
        /// </summary>
        /// <param name="update"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/UpdateTaskComment")]
        public HttpResponseMessage UpdateTaskComment(UpdateTaskCommentModel update)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                Task task = new Task();
                creator = db.Person.Where(a => a.UserName == update.Username && a.Password == update.Password && a.IsActive).FirstOrDefault();
                task = db.Task.FirstOrDefault(a => a.Id == update.Id);
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (task == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen feladat id: {update.Id}", System.Text.Encoding.Unicode) }; }
                if (creator.IsAdmin || task.Group.Leaders.Contains(creator))
                {


                    try
                    {
                        if (update.Comment!=null)
                        {
                            task.Comment = update.Comment;
                            db.SaveChanges();
                            var r = new HttpResponseMessage();
                            r.StatusCode = HttpStatusCode.OK;
                            return r;

                        }
                        else
                        {
                            var r = new HttpResponseMessage();
                            r.StatusCode = HttpStatusCode.BadRequest;
                            r.Content = new StringContent("A megjegyzés nem lehet null", System.Text.Encoding.Unicode);
                            return r;
                        }
                    }
                    catch (Exception ex)
                    {
                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.InternalServerError;
                        res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                        return res;
                    }
                }
                else
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.Unauthorized;
                    res.Content = new StringContent("Csak admin vagy csoportvezető fűzhet megjegyzést", System.Text.Encoding.Unicode);
                    return res;
                }
            }

        }
        /// <summary>
        /// feladat előrehaladásának frissítése, admin és csoportvezetők hívhatják.
        /// </summary>
        /// <param name="update"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/UpdateTaskProgress")]
        public HttpResponseMessage UpdateTaskProgress(UpdateProgressModel update)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                Task task = new Task();
                creator = db.Person.Where(a => a.UserName == update.Username && a.Password == update.Password && a.IsActive).FirstOrDefault();
                task = db.Task.FirstOrDefault(a => a.Id == update.Id);
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (task == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen feladat id: {update.Id}", System.Text.Encoding.Unicode) }; }
                if (creator.IsAdmin || task.Group.Leaders.Contains(creator) )
                {


                    try
                    {
                        if (update.Progress >= 0 && update.Progress <= 100)
                        {
                            task.Progress = update.Progress;
                            db.SaveChanges();
                            var r = new HttpResponseMessage();
                            r.StatusCode = HttpStatusCode.OK;
                            return r;

                        }
                        else
                        {
                            var r = new HttpResponseMessage();
                            r.StatusCode = HttpStatusCode.BadRequest;
                            r.Content = new StringContent("Az előrehaladásnak 0 és 100 között kell lennie", System.Text.Encoding.Unicode);
                            return r;
                        }
                    }
                    catch (Exception ex)
                    {
                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.InternalServerError;
                        res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                        return res;
                    }
                }
                else
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.Unauthorized;
                    return res;
                }
            }

        }
        /// <summary>
        /// részfeladat előrehaladásának módosítása, admin, csoportvezetők, részfeladathoz hozzárendelt felhasználók hívhatják.
        /// </summary>
        /// <param name="update"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/UpdateSubtaskProgress")]
        public HttpResponseMessage UpdateSubtaskProgress(UpdateProgressModel update)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                SubTask task = new SubTask();
                creator = db.Person.Where(a => a.UserName == update.Username && a.Password == update.Password && a.IsActive).FirstOrDefault();
                task = db.SubTask.FirstOrDefault(a => a.Id == update.Id);
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (task == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen részfeladat id: {update.Id}", System.Text.Encoding.Unicode) }; }
                if (creator.IsAdmin || task.ParentTask.Group.Leaders.Contains(creator) || task.Persons.Contains(creator))
                {


                    try
                    {

                        
                            if (update.Progress >= 0 && update.Progress <= 100)
                            {
                                task.Progress = update.Progress;
                                db.SaveChanges();
                                var r = new HttpResponseMessage();
                                r.StatusCode = HttpStatusCode.OK;
                                return r;

                            }
                            else
                            {
                                var r = new HttpResponseMessage();
                                r.StatusCode = HttpStatusCode.BadRequest;
                                r.Content = new StringContent("Az előrehaladásnak 0 és 100 között kell lennie", System.Text.Encoding.Unicode);
                                return r;
                            }
                        }
                    catch (Exception ex)
                    {
                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.InternalServerError;
                        res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                        return res;
                    }
                }
                else
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.Unauthorized;
                    return res;
                }
            }

        }
        /// <summary>
        /// felhasználó részfeladathoz rendelése, admin és csoportvezetők hívhatják.
        /// </summary>
        /// <param name="assign"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/AssignPersonToSubtask")]
        public HttpResponseMessage AssignPersonToSubtask(SubjectObjectModel assign)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                SubTask task = new SubTask();
                Person user = new Person();
                creator = db.Person.Where(a => a.UserName == assign.Username && a.Password == assign.Password && a.IsActive).FirstOrDefault();
                task = db.SubTask.FirstOrDefault(a => a.Id == assign.Object);
                user = db.Person.FirstOrDefault(a => a.Id == assign.Subject);
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (task == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen részfeladat id: {assign.Object}", System.Text.Encoding.Unicode) }; }
                if (task == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen felhasználó id: {assign.Subject}", System.Text.Encoding.Unicode) }; }

                if (creator.IsAdmin || task.ParentTask.Group.Leaders.Contains(creator))
                {


                    try
                    {


                        if (task.ParentTask.Group.Members.Contains(user))
                        {
                            if (!task.Persons.Contains(user))
                            {
                                task.Persons.Add(user);
                                db.SaveChanges();
                            }



                            var r = new HttpResponseMessage();
                            r.StatusCode = HttpStatusCode.OK;
                            return r;
                        }
                        else {
                            var r = new HttpResponseMessage();
                            r.StatusCode = HttpStatusCode.BadRequest;
                            r.Content = new StringContent("A felhasználó nem tagja a feladathoz tartozó csoportnak");
                            return r;
                        }
                        

                    }
                    catch (Exception ex)
                    {
                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.InternalServerError;
                        res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                        return res;
                    }
                }
                else
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.Unauthorized;
                    res.Content = new StringContent("Csak csoportvezető vagy adminisztázot adhat felhasználót részfeladathoz", System.Text.Encoding.Unicode);
                    return res;
                }
            }

        }
        /// <summary>
        /// felhasználó eltávolítása részfeladatból, admin és csoportvezetők hívhatják.
        /// </summary>
        /// <param name="assign"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/RemovePersonFromSubtask")]
        public HttpResponseMessage RemovePersonFromSubtask(SubjectObjectModel assign)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                SubTask task = new SubTask();
                Person user = new Person();
                creator = db.Person.Where(a => a.UserName == assign.Username && a.Password == assign.Password && a.IsActive).FirstOrDefault();
                task = db.SubTask.FirstOrDefault(a => a.Id == assign.Object);
                user = db.Person.FirstOrDefault(a => a.Id == assign.Subject);
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (task == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen részfeladat id: {assign.Object}", System.Text.Encoding.Unicode) }; }
                if (task == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen felhasználó id: {assign.Subject}", System.Text.Encoding.Unicode) }; }

                if (creator.IsAdmin || task.ParentTask.Group.Leaders.Contains(creator))
                {


                    try
                    {


                        if (task.Persons.Contains(user))
                        {

                            task.Persons.Remove(user);
                            db.SaveChanges();


                        }
                        var r = new HttpResponseMessage();
                        r.StatusCode = HttpStatusCode.OK;
                        return r;



                    }
                    catch (Exception ex)
                    {
                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.InternalServerError;
                        res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                        return res;
                    }
                }
                else
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.Unauthorized;
                    res.Content = new StringContent("Csak csoportvezető vagy adminisztázot törölhet felhasználót részfeladatból", System.Text.Encoding.Unicode);
                    return res;
                }
            }

        }

        /// <summary>
        /// Felhasználó hozzárendelése feladathoz egyénileg. admin, csoportvezetők hívhatják.
        /// </summary>
        /// <param name="assign"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/AssignPersonToTask")]
        public HttpResponseMessage AssignPersonToTask(SubjectObjectModel assign)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                Task task = new Task();
                Person user = new Person();
                creator = db.Person.Where(a => a.UserName == assign.Username && a.Password == assign.Password && a.IsActive).FirstOrDefault();
                task = db.Task.FirstOrDefault(a => a.Id == assign.Object);
                user = db.Person.FirstOrDefault(a => a.Id == assign.Subject);
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (task == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen feladat id: {assign.Object}", System.Text.Encoding.Unicode) }; }
                if (task == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen felhasználó id: {assign.Subject}", System.Text.Encoding.Unicode) }; }

                if (creator.IsAdmin || task.Group.Leaders.Contains(creator))
                {


                    try
                    {


                        if (!task.AssignedPersons.Contains(user))
                        {

                            task.AssignedPersons.Add(user);
                            db.SaveChanges();


                        }
                        var r = new HttpResponseMessage();
                        r.StatusCode = HttpStatusCode.OK;
                        return r;



                    }
                    catch (Exception ex)
                    {
                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.InternalServerError;
                        res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                        return res;
                    }
                }
                else
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.Unauthorized;
                    res.Content = new StringContent("Csak adminisztázot adhat felhasználót feladathoz", System.Text.Encoding.Unicode);
                    return res;
                }
            }

        }
        /// <summary>
        /// Felasználó eltávolítása egényi feladatból, admin és csoportvezetők hívhatják.
        /// </summary>
        /// <param name="assign"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/RemovePersonFromTask")]
        public HttpResponseMessage RemovePersonFromTask(SubjectObjectModel assign)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                Task task = new Task();
                Person user = new Person();
                creator = db.Person.Where(a => a.UserName == assign.Username && a.Password == assign.Password && a.IsActive).FirstOrDefault();
                task = db.Task.FirstOrDefault(a => a.Id == assign.Object);
                user = db.Person.FirstOrDefault(a => a.Id == assign.Subject);
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (task == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen részfeladat id: {assign.Object}", System.Text.Encoding.Unicode) }; }
                if (task == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen felhasználó id: {assign.Subject}", System.Text.Encoding.Unicode) }; }

                if (creator.IsAdmin || task.Group.Leaders.Contains(creator))
                {


                    try
                    {


                        if (task.AssignedPersons.Contains(user))
                        {

                            task.AssignedPersons.Remove(user);
                            db.SaveChanges();


                        }
                        var r = new HttpResponseMessage();
                        r.StatusCode = HttpStatusCode.OK;
                        return r;



                    }
                    catch (Exception ex)
                    {
                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.InternalServerError;
                        res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                        return res;
                    }
                }
                else
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.Unauthorized;
                    res.Content = new StringContent("Csak adminisztázor törölhet felhasználót feladatból", System.Text.Encoding.Unicode);
                    return res;
                }
            }

        }
    }
}

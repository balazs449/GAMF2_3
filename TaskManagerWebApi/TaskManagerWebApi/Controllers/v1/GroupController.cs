﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TaskManagerWebApi.Models;

namespace TaskManagerWebApi.Controllers.v1
{
    public class GroupController : ApiController
    {

        /// <summary>
        /// Csoport letörlése csak admin hívhatja.
        /// </summary>
        /// <param name="getgroup"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/DeleteGroup")]
        public HttpResponseMessage DeleteGroup(IdModel getgroup)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                Group group = new Group();
                creator = db.Person.Where(a => a.UserName == getgroup.Username && a.Password == getgroup.Password && a.IsActive && a.IsAdmin).FirstOrDefault();
                group = db.Group.FirstOrDefault(a => a.Id == getgroup.Id);
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (group == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nincs ilyen felhasználó id: {getgroup.Id}", System.Text.Encoding.Unicode) }; }



                try
                {
                    group.IsActive = false;
                    group.Leaders.Clear();
                    group.Members.Clear();
                    group.Tasks.Clear();
                    

                    db.SaveChanges();
                    var r = new HttpResponseMessage();
                    r.StatusCode = HttpStatusCode.OK;
                    return r;
                }
                catch (Exception ex)
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                    return res;
                }

            }

        }
        /// <summary>
        /// Új csoport létrehzása, csak admin hívhatja
        /// </summary>
        /// <param name="newGroup"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/newgroup")]
        public HttpResponseMessage NewGroup(NewGroupModel newGroup)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                creator = db.Person.Where(a => a.UserName == newGroup.Username && a.Password == newGroup.Password && a.IsActive).FirstOrDefault();
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (creator.IsAdmin)
                {


                    try
                    {
                        Group group = new Group();
                        group.Creator = creator;
                        group.Name = newGroup.Name;
                        group.IsActive = true;

                        db.Group.Add(group);
                        db.SaveChanges();
                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.Created;
                        return res;
                    }
                    catch (Exception ex)
                    {
                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.InternalServerError;
                        res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                        return res;
                    }




                }
                else
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.Unauthorized;
                    return res;
                }
            }

        }
        /// <summary>
        /// Összes csoport lehívása, csak admin hívhatja.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/getallgroups")]
        public HttpResponseMessage GetAllGroups(LoginModel login)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                creator = db.Person.Where(a => a.UserName == login.Username && a.Password == login.Password && a.IsActive).FirstOrDefault();
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                if (creator.IsAdmin)
                {


                    try
                    {
                        var r = new HttpResponseMessage();
                        r.Content = new StringContent(JsonConvert.SerializeObject(db.Group.Where(a => a.IsActive).ToList()));



                        r.StatusCode = HttpStatusCode.OK;
                        return r;
                    }
                    catch (Exception ex)
                    {
                        var res = new HttpResponseMessage();
                        res.StatusCode = HttpStatusCode.InternalServerError;
                        res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                        return res;
                    }




                }
                else
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.Unauthorized;
                    return res;
                }
            }

        }
        /// <summary>
        /// Saját csoportok lehívása, bárki hívhatka.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/getowngroups")]
        public HttpResponseMessage GetOwnGroups(LoginModel login)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                creator = db.Person.Where(a => a.UserName == login.Username && a.Password == login.Password && a.IsActive).FirstOrDefault();
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                var groups = creator.Groups.Where(a=>a.IsActive).ToList();


                    try
                    {
                        var r = new HttpResponseMessage();
                        r.Content = new StringContent(JsonConvert.SerializeObject(groups));



                        r.StatusCode = HttpStatusCode.OK;
                        return r;
                    }
                    catch (Exception ex)
                    {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                    return res;
                }





            }

        }
        /// <summary>
        /// Cskoport lekérdezése ID alapján, admin, tagok, vezetpk hívhatják.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/GetGroupById")]
        public HttpResponseMessage GetGroupById(IdModel login)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                creator = db.Person.Where(a => a.UserName == login.Username && a.Password == login.Password && a.IsActive).FirstOrDefault();
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }
                var group = db.Group.FirstOrDefault(a => a.Id == login.Id);
                if (group == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.NotFound, Content = new StringContent($"Nem található ilyen csoport: {login.Id}", System.Text.Encoding.Unicode) }; }
                if (!creator.IsAdmin && !group.Members.Contains(creator) && !group.Leaders.Contains(creator))
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Unauthorized", System.Text.Encoding.Unicode) };
                }

                try
                {
                    var r = new HttpResponseMessage();
                    r.Content = new StringContent(JsonConvert.SerializeObject(group));



                    r.StatusCode = HttpStatusCode.OK;
                    return r;
                }
                catch (Exception ex)
                {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                    return res;
                }





            }

        }
        /// <summary>
        /// Saját vezetett csoportok lehívása, bárki hívhatja.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/getleadgroups")]
        public HttpResponseMessage GetLeadGroups(LoginModel login)
        {

            using (TaskModelContainer db = new TaskModelContainer())
            {
                Person creator = new Person();
                creator = db.Person.Where(a => a.UserName == login.Username && a.Password == login.Password && a.IsActive).FirstOrDefault();
                if (creator == null) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent("Rossz felhasználónév vagy jelszó", System.Text.Encoding.Unicode) }; }



                    try
                    {
                        var r = new HttpResponseMessage();
                        r.Content = new StringContent(JsonConvert.SerializeObject(creator.LeadGroups.ToList()));



                        r.StatusCode = HttpStatusCode.OK;
                        return r;
                    }
                    catch (Exception ex)
                    {
                    var res = new HttpResponseMessage();
                    res.StatusCode = HttpStatusCode.InternalServerError;
                    res.Content = new StringContent(ex.Message, System.Text.Encoding.Unicode);
                    return res;
                }





            }

        }
    }
}


-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/19/2018 17:25:02
-- Generated from EDMX file: C:\Users\Ricsi\Documents\Munka\Task\TaskManagerWebApi\TaskManagerWebApi\Models\TaskModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [huszaral_mobil];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_PersonTask]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Task] DROP CONSTRAINT [FK_PersonTask];
GO
IF OBJECT_ID(N'[dbo].[FK_TaskSubTask]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SubTask] DROP CONSTRAINT [FK_TaskSubTask];
GO
IF OBJECT_ID(N'[dbo].[FK_PersonGroup]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Group] DROP CONSTRAINT [FK_PersonGroup];
GO
IF OBJECT_ID(N'[dbo].[FK_PersonSkill_Person]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[HasSkill] DROP CONSTRAINT [FK_PersonSkill_Person];
GO
IF OBJECT_ID(N'[dbo].[FK_PersonSkill_Skill]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[HasSkill] DROP CONSTRAINT [FK_PersonSkill_Skill];
GO
IF OBJECT_ID(N'[dbo].[FK_PersonTask1_Person]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AssignedToTask] DROP CONSTRAINT [FK_PersonTask1_Person];
GO
IF OBJECT_ID(N'[dbo].[FK_PersonTask1_Task]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AssignedToTask] DROP CONSTRAINT [FK_PersonTask1_Task];
GO
IF OBJECT_ID(N'[dbo].[FK_PersonGroup1_Person]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GroupMember] DROP CONSTRAINT [FK_PersonGroup1_Person];
GO
IF OBJECT_ID(N'[dbo].[FK_PersonGroup1_Group]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GroupMember] DROP CONSTRAINT [FK_PersonGroup1_Group];
GO
IF OBJECT_ID(N'[dbo].[FK_PersonGroup2_Person]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GroupLeader] DROP CONSTRAINT [FK_PersonGroup2_Person];
GO
IF OBJECT_ID(N'[dbo].[FK_PersonGroup2_Group]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GroupLeader] DROP CONSTRAINT [FK_PersonGroup2_Group];
GO
IF OBJECT_ID(N'[dbo].[FK_GroupTask]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Task] DROP CONSTRAINT [FK_GroupTask];
GO
IF OBJECT_ID(N'[dbo].[FK_PersonSubTask_Person]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PersonSubTask] DROP CONSTRAINT [FK_PersonSubTask_Person];
GO
IF OBJECT_ID(N'[dbo].[FK_PersonSubTask_SubTask]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PersonSubTask] DROP CONSTRAINT [FK_PersonSubTask_SubTask];
GO
IF OBJECT_ID(N'[dbo].[FK_PersonPerson]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Person] DROP CONSTRAINT [FK_PersonPerson];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Person]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Person];
GO
IF OBJECT_ID(N'[dbo].[Task]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Task];
GO
IF OBJECT_ID(N'[dbo].[SubTask]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SubTask];
GO
IF OBJECT_ID(N'[dbo].[Group]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Group];
GO
IF OBJECT_ID(N'[dbo].[Skill]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Skill];
GO
IF OBJECT_ID(N'[dbo].[HasSkill]', 'U') IS NOT NULL
    DROP TABLE [dbo].[HasSkill];
GO
IF OBJECT_ID(N'[dbo].[AssignedToTask]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AssignedToTask];
GO
IF OBJECT_ID(N'[dbo].[GroupMember]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GroupMember];
GO
IF OBJECT_ID(N'[dbo].[GroupLeader]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GroupLeader];
GO
IF OBJECT_ID(N'[dbo].[PersonSubTask]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PersonSubTask];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Person'
CREATE TABLE [dbo].[Person] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(100)  NOT NULL,
    [Email] nvarchar(100)  NOT NULL,
    [UserName] nvarchar(50)  NOT NULL,
    [IsAdmin] bit  NOT NULL,
    [IsActive] bit  NOT NULL,
    [Password] nvarchar(max)  NOT NULL,
    [Creator_Id] int  NULL
);
GO

-- Creating table 'Task'
CREATE TABLE [dbo].[Task] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(150)  NOT NULL,
    [CreationDate] datetime  NOT NULL,
    [StartDate] datetime  NOT NULL,
    [Deadline] datetime  NOT NULL,
    [Progress] int  NOT NULL,
    [IsActive] bit  NOT NULL,
    [Comment] nvarchar(max)  NOT NULL,
    [Creator_Id] int  NOT NULL,
    [Group_Id] int  NULL
);
GO

-- Creating table 'SubTask'
CREATE TABLE [dbo].[SubTask] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(255)  NOT NULL,
    [Progress] int  NOT NULL,
    [IsActive] bit  NOT NULL,
    [ParentTask_Id] int  NULL
);
GO

-- Creating table 'Group'
CREATE TABLE [dbo].[Group] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(100)  NOT NULL,
    [IsActive] bit  NOT NULL,
    [Creator_Id] int  NOT NULL
);
GO

-- Creating table 'Skill'
CREATE TABLE [dbo].[Skill] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(100)  NOT NULL
);
GO

-- Creating table 'HasSkill'
CREATE TABLE [dbo].[HasSkill] (
    [Persons_Id] int  NOT NULL,
    [Skills_Id] int  NOT NULL
);
GO

-- Creating table 'AssignedToTask'
CREATE TABLE [dbo].[AssignedToTask] (
    [AssignedPersons_Id] int  NOT NULL,
    [Tasks_Id] int  NOT NULL
);
GO

-- Creating table 'GroupMember'
CREATE TABLE [dbo].[GroupMember] (
    [Members_Id] int  NOT NULL,
    [Groups_Id] int  NOT NULL
);
GO

-- Creating table 'GroupLeader'
CREATE TABLE [dbo].[GroupLeader] (
    [Leaders_Id] int  NOT NULL,
    [LeadGroups_Id] int  NOT NULL
);
GO

-- Creating table 'PersonSubTask'
CREATE TABLE [dbo].[PersonSubTask] (
    [Persons_Id] int  NOT NULL,
    [SubTasks_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Person'
ALTER TABLE [dbo].[Person]
ADD CONSTRAINT [PK_Person]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Task'
ALTER TABLE [dbo].[Task]
ADD CONSTRAINT [PK_Task]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SubTask'
ALTER TABLE [dbo].[SubTask]
ADD CONSTRAINT [PK_SubTask]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Group'
ALTER TABLE [dbo].[Group]
ADD CONSTRAINT [PK_Group]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Skill'
ALTER TABLE [dbo].[Skill]
ADD CONSTRAINT [PK_Skill]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Persons_Id], [Skills_Id] in table 'HasSkill'
ALTER TABLE [dbo].[HasSkill]
ADD CONSTRAINT [PK_HasSkill]
    PRIMARY KEY CLUSTERED ([Persons_Id], [Skills_Id] ASC);
GO

-- Creating primary key on [AssignedPersons_Id], [Tasks_Id] in table 'AssignedToTask'
ALTER TABLE [dbo].[AssignedToTask]
ADD CONSTRAINT [PK_AssignedToTask]
    PRIMARY KEY CLUSTERED ([AssignedPersons_Id], [Tasks_Id] ASC);
GO

-- Creating primary key on [Members_Id], [Groups_Id] in table 'GroupMember'
ALTER TABLE [dbo].[GroupMember]
ADD CONSTRAINT [PK_GroupMember]
    PRIMARY KEY CLUSTERED ([Members_Id], [Groups_Id] ASC);
GO

-- Creating primary key on [Leaders_Id], [LeadGroups_Id] in table 'GroupLeader'
ALTER TABLE [dbo].[GroupLeader]
ADD CONSTRAINT [PK_GroupLeader]
    PRIMARY KEY CLUSTERED ([Leaders_Id], [LeadGroups_Id] ASC);
GO

-- Creating primary key on [Persons_Id], [SubTasks_Id] in table 'PersonSubTask'
ALTER TABLE [dbo].[PersonSubTask]
ADD CONSTRAINT [PK_PersonSubTask]
    PRIMARY KEY CLUSTERED ([Persons_Id], [SubTasks_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Creator_Id] in table 'Task'
ALTER TABLE [dbo].[Task]
ADD CONSTRAINT [FK_PersonTask]
    FOREIGN KEY ([Creator_Id])
    REFERENCES [dbo].[Person]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonTask'
CREATE INDEX [IX_FK_PersonTask]
ON [dbo].[Task]
    ([Creator_Id]);
GO

-- Creating foreign key on [ParentTask_Id] in table 'SubTask'
ALTER TABLE [dbo].[SubTask]
ADD CONSTRAINT [FK_TaskSubTask]
    FOREIGN KEY ([ParentTask_Id])
    REFERENCES [dbo].[Task]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TaskSubTask'
CREATE INDEX [IX_FK_TaskSubTask]
ON [dbo].[SubTask]
    ([ParentTask_Id]);
GO

-- Creating foreign key on [Creator_Id] in table 'Group'
ALTER TABLE [dbo].[Group]
ADD CONSTRAINT [FK_PersonGroup]
    FOREIGN KEY ([Creator_Id])
    REFERENCES [dbo].[Person]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonGroup'
CREATE INDEX [IX_FK_PersonGroup]
ON [dbo].[Group]
    ([Creator_Id]);
GO

-- Creating foreign key on [Persons_Id] in table 'HasSkill'
ALTER TABLE [dbo].[HasSkill]
ADD CONSTRAINT [FK_PersonSkill_Person]
    FOREIGN KEY ([Persons_Id])
    REFERENCES [dbo].[Person]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Skills_Id] in table 'HasSkill'
ALTER TABLE [dbo].[HasSkill]
ADD CONSTRAINT [FK_PersonSkill_Skill]
    FOREIGN KEY ([Skills_Id])
    REFERENCES [dbo].[Skill]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonSkill_Skill'
CREATE INDEX [IX_FK_PersonSkill_Skill]
ON [dbo].[HasSkill]
    ([Skills_Id]);
GO

-- Creating foreign key on [AssignedPersons_Id] in table 'AssignedToTask'
ALTER TABLE [dbo].[AssignedToTask]
ADD CONSTRAINT [FK_PersonTask1_Person]
    FOREIGN KEY ([AssignedPersons_Id])
    REFERENCES [dbo].[Person]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Tasks_Id] in table 'AssignedToTask'
ALTER TABLE [dbo].[AssignedToTask]
ADD CONSTRAINT [FK_PersonTask1_Task]
    FOREIGN KEY ([Tasks_Id])
    REFERENCES [dbo].[Task]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonTask1_Task'
CREATE INDEX [IX_FK_PersonTask1_Task]
ON [dbo].[AssignedToTask]
    ([Tasks_Id]);
GO

-- Creating foreign key on [Members_Id] in table 'GroupMember'
ALTER TABLE [dbo].[GroupMember]
ADD CONSTRAINT [FK_PersonGroup1_Person]
    FOREIGN KEY ([Members_Id])
    REFERENCES [dbo].[Person]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Groups_Id] in table 'GroupMember'
ALTER TABLE [dbo].[GroupMember]
ADD CONSTRAINT [FK_PersonGroup1_Group]
    FOREIGN KEY ([Groups_Id])
    REFERENCES [dbo].[Group]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonGroup1_Group'
CREATE INDEX [IX_FK_PersonGroup1_Group]
ON [dbo].[GroupMember]
    ([Groups_Id]);
GO

-- Creating foreign key on [Leaders_Id] in table 'GroupLeader'
ALTER TABLE [dbo].[GroupLeader]
ADD CONSTRAINT [FK_PersonGroup2_Person]
    FOREIGN KEY ([Leaders_Id])
    REFERENCES [dbo].[Person]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [LeadGroups_Id] in table 'GroupLeader'
ALTER TABLE [dbo].[GroupLeader]
ADD CONSTRAINT [FK_PersonGroup2_Group]
    FOREIGN KEY ([LeadGroups_Id])
    REFERENCES [dbo].[Group]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonGroup2_Group'
CREATE INDEX [IX_FK_PersonGroup2_Group]
ON [dbo].[GroupLeader]
    ([LeadGroups_Id]);
GO

-- Creating foreign key on [Group_Id] in table 'Task'
ALTER TABLE [dbo].[Task]
ADD CONSTRAINT [FK_GroupTask]
    FOREIGN KEY ([Group_Id])
    REFERENCES [dbo].[Group]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GroupTask'
CREATE INDEX [IX_FK_GroupTask]
ON [dbo].[Task]
    ([Group_Id]);
GO

-- Creating foreign key on [Persons_Id] in table 'PersonSubTask'
ALTER TABLE [dbo].[PersonSubTask]
ADD CONSTRAINT [FK_PersonSubTask_Person]
    FOREIGN KEY ([Persons_Id])
    REFERENCES [dbo].[Person]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [SubTasks_Id] in table 'PersonSubTask'
ALTER TABLE [dbo].[PersonSubTask]
ADD CONSTRAINT [FK_PersonSubTask_SubTask]
    FOREIGN KEY ([SubTasks_Id])
    REFERENCES [dbo].[SubTask]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonSubTask_SubTask'
CREATE INDEX [IX_FK_PersonSubTask_SubTask]
ON [dbo].[PersonSubTask]
    ([SubTasks_Id]);
GO

-- Creating foreign key on [Creator_Id] in table 'Person'
ALTER TABLE [dbo].[Person]
ADD CONSTRAINT [FK_PersonPerson]
    FOREIGN KEY ([Creator_Id])
    REFERENCES [dbo].[Person]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonPerson'
CREATE INDEX [IX_FK_PersonPerson]
ON [dbo].[Person]
    ([Creator_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TaskManagerWebApi.Models
{
    [System.Runtime.Serialization.DataContract]
    public class IdModel
    {
        [System.Runtime.Serialization.DataMember]
        [Required]
        public string Username { get; set; }
        [System.Runtime.Serialization.DataMember]
        [Required]
        public string Password { get; set; }
        [System.Runtime.Serialization.DataMember]
        [Required]
        public int Id { get; set; }
    }
}
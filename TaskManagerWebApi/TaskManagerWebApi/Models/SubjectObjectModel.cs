﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskManagerWebApi.Models
{
    [System.Runtime.Serialization.DataContract]
    public class SubjectObjectModel
    {
        [System.Runtime.Serialization.DataMember]
        [System.ComponentModel.DataAnnotations.Required]
        public string Username { get; set; }
        [System.Runtime.Serialization.DataMember]
        [System.ComponentModel.DataAnnotations.Required]
        public string Password { get; set; }
        [System.Runtime.Serialization.DataMember]
        [System.ComponentModel.DataAnnotations.Required]
        public int Subject { get; set; }
        [System.Runtime.Serialization.DataMember]
        [System.ComponentModel.DataAnnotations.Required]
        public int Object { get; set; }
    }
}
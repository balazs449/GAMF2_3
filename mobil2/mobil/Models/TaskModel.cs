﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskManagerApp.Models
{
    [System.Runtime.Serialization.DataContract]
    class TaskModel
    {
        [System.Runtime.Serialization.DataMember]
        public int Id { get; set; }
        [System.Runtime.Serialization.DataMember]
        public string Title { get; set; }
        [System.Runtime.Serialization.DataMember]
        public System.DateTime CreationDate { get; set; }
        [System.Runtime.Serialization.DataMember]
        public System.DateTime StartDate { get; set; }
        [System.Runtime.Serialization.DataMember]
        public System.DateTime Deadline { get; set; }
        [System.Runtime.Serialization.DataMember]
        public int Progress { get; set; }
        [System.Runtime.Serialization.DataMember]
        public virtual GroupModel Group { get; set; }
        [System.Runtime.Serialization.DataMember]
        public string Comment { get; set; }
        [System.Runtime.Serialization.DataMember]
        public virtual List<SubTaskModel> Subtasks { get; set; }
    }
}

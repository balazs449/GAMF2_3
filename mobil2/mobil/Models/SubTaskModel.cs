﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskManagerApp.Models
{
    [System.Runtime.Serialization.DataContract]
    class SubTaskModel
    {
        [System.Runtime.Serialization.DataMember]
        public int Id { get; set; }
        [System.Runtime.Serialization.DataMember]
        public string Title { get; set; }
        [System.Runtime.Serialization.DataMember]
        public int Progress { get; set; }
    }
}

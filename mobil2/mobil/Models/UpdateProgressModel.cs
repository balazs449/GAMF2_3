﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskManagerApp.Models
{
    class UpdateProgressModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public int Progress { get; set; }
        public int Id { get; set; }
    }
}

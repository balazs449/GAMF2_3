﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskManagerApp.Models
{
    [System.Runtime.Serialization.DataContract]
    class GroupModel
    {
        [System.Runtime.Serialization.DataMember]
        public int Id { get; set; }
        [System.Runtime.Serialization.DataMember]
        public string Name { get; set; }
        [System.Runtime.Serialization.DataMember]
        public virtual ICollection<PersonModel> Members { get; set; }
        [System.Runtime.Serialization.DataMember]
        public virtual ICollection<PersonModel> Leaders { get; set; }
        [System.Runtime.Serialization.DataMember]
        public virtual ICollection<TaskModel> Tasks { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskManagerApp.Models
{
    class NewSubtaskModel
    {
        public string Password { get; set; }
        public string Username { get; set; }
        public string Title { get; set; }
        public int ParentId { get; set; }
    }
}

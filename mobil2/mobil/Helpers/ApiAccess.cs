﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;


using TaskManagerApp.Models;

namespace TaskManagerApp.Helpers
{
    class ApiAccess
    {
        private LoginModel user;
        private HttpClient client;
        private string Endpoint;

        public enum resultType
        {
            OK,
            WrongLogin,
            Created,
            Unathorized,
            Error
        }

        public class Result
        {
            public resultType ResultType;
            public string Message = "No Message";
            public object ReturnObject;
        }

        public ApiAccess(string username, string password, string endpoint)
        {
            user = new LoginModel();
            user.Username = username;
            user.Password = password;
            Endpoint = endpoint;
            client = new HttpClient();
        }
        /// <summary>
        /// bejelentkezést ellenörző művelet, tényleges beléptetés nincs mögötte. Bárki hívhatja.
        /// </summary>
        /// <returns></returns>
        public async Task<Result> Login()
        {
            var values = user;

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "login"), content);

                if (response.IsSuccessStatusCode)
                {
                    return new Result() { ResultType = resultType.OK };
                }
                else
                {
                    string mes = await response.Content.ReadAsStringAsync();
                    return new Result() { ResultType = resultType.Error, Message = mes };
                }


            }
            catch (Exception ex)
            {
                return new Result() { ResultType = resultType.Error };
            }
        }
        /// <summary>
        /// Új felhasználó létrehozása, csak admin hívhatja.
        /// </summary>
        /// <param name="admin"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="email"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<Result> NewUser(bool admin, string username, string password, string email, string name)
        {
            var values = new NewUserModel() { Username = user.Username, Password = user.Password, NewUserAdmin = admin, NewUserEmail = email, NewUserPassword = password, NewUserName = name, NewUserUsername = username };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "newuser"), content);

                if (response.IsSuccessStatusCode)
                {
                    return new Result() { ResultType = resultType.OK };
                }
                else
                {
                    string mes = await response.Content.ReadAsStringAsync();
                    return new Result() { ResultType = resultType.Error, Message = mes };
                }
            }
            catch (Exception ex)
            {
                return new Result() { ResultType = resultType.Error };
            }
        }
        /// <summary>
        /// Az összes felhasználót visszadó metódus, bárki hívhatja.
        /// </summary>
        /// <returns></returns>
        public async Task<Result> GetAllUsers()
        {
            var values = new LoginModel() { Username = user.Username, Password = user.Password };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "GetAllUsers"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();

                    var res = JsonConvert.DeserializeObject<List<PersonModel>>(responseString);

                    return new Result { ResultType = resultType.OK, ReturnObject = res };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// Az adminokat visszaadó metódus, bárki hívhatja.
        /// </summary>
        /// <returns></returns>
        public async Task<Result> GetAllAdmins()
        {
            var values = new LoginModel() { Username = user.Username, Password = user.Password };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "GetAllAdmins"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();

                    var res = JsonConvert.DeserializeObject<List<PersonModel>>(responseString);

                    return new Result { ResultType = resultType.OK, ReturnObject = res };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        ///  A csoport vezetőket visszaadó metódus, bárki hívhatja.
        /// </summary>
        /// <returns></returns>
        public async Task<Result> GetAllLeaders()
        {
            var values = new LoginModel() { Username = user.Username, Password = user.Password };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "GetAllLeaders"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();

                    var res = JsonConvert.DeserializeObject<List<PersonModel>>(responseString);

                    return new Result { ResultType = resultType.OK, ReturnObject = res };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        ///  A képességeket visszaadó metódus, bárki hívhatja.
        /// </summary>
        /// <returns></returns>
        public async Task<Result> GetAllSkills()
        {
            var values = new LoginModel() { Username = user.Username, Password = user.Password };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "GetAllSkills"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();

                    var res = JsonConvert.DeserializeObject<List<SkillModel>>(responseString);

                    return new Result { ResultType = resultType.OK, ReturnObject = res };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// A saját képességeket visszaadó metódus, bárki hívhatja
        /// </summary>
        /// <returns></returns>
        public async Task<Result> GetOwnSkills()
        {
            var values = new LoginModel() { Username = user.Username, Password = user.Password };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "GetOwnSkills"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();

                    var res = JsonConvert.DeserializeObject<List<SkillModel>>(responseString);

                    return new Result { ResultType = resultType.OK, ReturnObject = res };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// Képességet felhasználóhoz adó metódus, csak admi hívhatja.
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="SkillId"></param>
        /// <returns></returns>
        public async Task<Result> AddSkillToUser(int UserId, int SkillId)
        {
            var values = new SubjectObjectModel() { Username = user.Username, Password = user.Password, Subject = UserId, Object = SkillId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "AddSkillToUser"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();


                    return new Result { ResultType = resultType.OK };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// Képességet felhasználótól eltávolító metódus, csak admin hívhatja.
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="SkillId"></param>
        /// <returns></returns>
        public async Task<Result> RemoveSkillFromUser(int UserId, int SkillId)
        {
            var values = new SubjectObjectModel() { Username = user.Username, Password = user.Password, Subject = UserId, Object = SkillId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "RemoveSkillFromUser"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();


                    return new Result { ResultType = resultType.OK };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// Felhasználót csoporthoz adó metódus, csak admin hívhatja.
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="GroupId"></param>
        /// <returns></returns>
        public async Task<Result> AddUserToGroup(int UserId, int GroupId)
        {
            var values = new SubjectObjectModel() { Username = user.Username, Password = user.Password, Subject = UserId, Object = GroupId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "AddUserToGroup"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();


                    return new Result { ResultType = resultType.OK };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// Felhasználót csoportból eltávolító metódus, csak admin hívhatja.
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="GroupId"></param>
        /// <returns></returns>
        public async Task<Result> RemoveUserFromGroup(int UserId, int GroupId)
        {
            var values = new SubjectObjectModel() { Username = user.Username, Password = user.Password, Subject = UserId, Object = GroupId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "RemoveUserFromGroup"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();


                    return new Result { ResultType = resultType.OK };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// Csoportvezető csoporthoz adása, csak admin hívhatja.
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="GroupId"></param>
        /// <returns></returns>
        public async Task<Result> AddLeaderToGroup(int UserId, int GroupId)
        {
            var values = new SubjectObjectModel() { Username = user.Username, Password = user.Password, Subject = UserId, Object = GroupId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "AddLeaderToGroup"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();


                    return new Result { ResultType = resultType.OK };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// Csoportvezető csoportból eltávolítása, csak admin hívhatja.
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="GroupId"></param>
        /// <returns></returns>
        public async Task<Result> RemoveLeaderFromGroup(int UserId, int GroupId)
        {
            var values = new SubjectObjectModel() { Username = user.Username, Password = user.Password, Subject = UserId, Object = GroupId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "RemoveLeaderFromGroup"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();


                    return new Result { ResultType = resultType.OK };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        ///  Új képesség felvétele, csak admin hívatja.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<Result> NewSkill(string name)
        {
            var values = new NameModel() { Username = user.Username, Password = user.Password, Name = name };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "NewSkill"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();


                    return new Result { ResultType = resultType.OK };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// Képsség letörlése, csak admin hívhatja.
        /// </summary>
        /// <param name="SkillId"></param>
        /// <returns></returns>
        public async Task<Result> DeleteSkill(int SkillId)
        {
            var values = new IdModel() { Username = user.Username, Password = user.Password, Id = SkillId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "DeleteSkill"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();


                    return new Result { ResultType = resultType.OK };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// Felhasználó id alapján való lekérdezése, bárki hívhatja.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public async Task<Result> GetUserById(int UserId)
        {
            var values = new IdModel() { Username = user.Username, Password = user.Password, Id = UserId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "GetUserById"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();

                    var res = JsonConvert.DeserializeObject<PersonModel>(responseString);


                    return new Result { ResultType = resultType.OK, ReturnObject = res };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// Felhasználó lekérdezése felhasználónév alapján bárki hívhatja.
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public async Task<Result> GetUserByUsername(string username)
        {
            var values = new NameModel() { Username = user.Username, Password = user.Password, Name = username };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "GetUserByUsername"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();

                    var res = JsonConvert.DeserializeObject<PersonModel>(responseString);


                    return new Result { ResultType = resultType.OK, ReturnObject = res };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// Felhasználó letörlése, csak admin hívhatja.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public async Task<Result> DeleteUser(int UserId)
        {
            var values = new IdModel() { Username = user.Username, Password = user.Password, Id = UserId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "DeleteUser"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();



                    return new Result { ResultType = resultType.OK };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// Csoport törlése, csak admin hívhatja.
        /// </summary>
        /// <param name="GroupId"></param>
        /// <returns></returns>
        public async Task<Result> DeleteGroup(int GroupId)
        {
            var values = new IdModel() { Username = user.Username, Password = user.Password, Id = GroupId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "DeleteGroup"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();



                    return new Result { ResultType = resultType.OK };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// Új csoport létrehozása, csak admin hívhatja.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<Result> NewGroup(string name)
        {
            var values = new NewGroupModel() { Username = user.Username, Password = user.Password, Name = name };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "NewGroup"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();



                    return new Result { ResultType = resultType.OK };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// Összes csoport lekérdezése, csak admin hívhatja.
        /// </summary>
        /// <returns></returns>
        public async Task<Result> GetAllGroups()
        {
            var values = new LoginModel() { Username = user.Username, Password = user.Password };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "GetAllGroups"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();

                    var res = JsonConvert.DeserializeObject<List<GroupModel>>(responseString);

                    return new Result { ResultType = resultType.OK, ReturnObject = res };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// GetGroupsByUser
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public async Task<Result> GetGroupsByUser(int UserId)
        {
            var values = new IdModel() { Username = user.Username, Password = user.Password, Id = UserId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "GetGroupsByUser"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();

                    var res = JsonConvert.DeserializeObject<List<GroupModel>>(responseString);

                    return new Result { ResultType = resultType.OK, ReturnObject = res };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// Saját csoportak lekérdezése, bárki hívhatja
        /// </summary>
        /// <returns></returns>
        public async Task<Result> GetOwnGroups()
        {
            var values = new LoginModel() { Username = user.Username, Password = user.Password };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "GetOwnGroups"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();

                    var res = JsonConvert.DeserializeObject<List<GroupModel>>(responseString);

                    return new Result { ResultType = resultType.OK, ReturnObject = res };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// Saját vezetett csoportok lehívása, bárki hívhatja.
        /// </summary>
        /// <returns></returns>
        public async Task<Result> GetLeadGroups()
        {
            var values = new LoginModel() { Username = user.Username, Password = user.Password };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "GetLeadGroups"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();

                    var res = JsonConvert.DeserializeObject<List<GroupModel>>(responseString);

                    return new Result { ResultType = resultType.OK, ReturnObject = res };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// Részfeladat törlése, csak admin vagy adott csoport vezetője hívhatja.
        /// </summary>
        /// <param name="subtaskId"></param>
        /// <returns></returns>
        public async Task<Result> DeleteSubTask(int subtaskId)
        {
            var values = new IdModel() { Username = user.Username, Password = user.Password, Id = subtaskId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "DeleteSubTask"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();


                    return new Result { ResultType = resultType.OK };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        ///  Feladat törlése, csak admin hívhatja.
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public async Task<Result> DeleteTask(int taskId)
        {
            var values = new IdModel() { Username = user.Username, Password = user.Password, Id = taskId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "DeleteTask"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();


                    return new Result { ResultType = resultType.OK };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// Új feladat létrehozása, cask admin hívhatja.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="start"></param>
        /// <param name="deadline"></param>
        /// <returns></returns>
        public async Task<Result> NewTask(string title, DateTime start, DateTime deadline)
        {
            var values = new NewTaskModel() { Username = user.Username, Password = user.Password, StartDate = start, Deadline = deadline, Title = title };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "newtask"), content);

                if (response.IsSuccessStatusCode)
                {
                    return new Result() { ResultType = resultType.OK };
                }
                else
                {
                    string mes = await response.Content.ReadAsStringAsync();
                    return new Result() { ResultType = resultType.Error, Message = mes };
                }
            }
            catch (Exception ex)
            {
                return new Result() { ResultType = resultType.Error };
            }
        }
        /// <summary>
        /// Új feladat létrehozása, csak admin hívhatja.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="start"></param>
        /// <param name="deadline"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        public async Task<Result> NewTask(string title, DateTime start, DateTime deadline, string comment)
        {
            var values = new NewTaskModel() { Username = user.Username, Password = user.Password, StartDate = start, Deadline = deadline, Title = title, Comment = comment };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "newtask"), content);

                if (response.IsSuccessStatusCode)
                {
                    return new Result() { ResultType = resultType.OK };
                }
                else
                {
                    string mes = await response.Content.ReadAsStringAsync();
                    return new Result() { ResultType = resultType.Error, Message = mes };
                }
            }
            catch (Exception ex)
            {
                return new Result() { ResultType = resultType.Error };
            }
        }
        /// <summary>
        /// Összes feladat lekérdezése, csak admin hívhatja.
        /// </summary>
        /// <returns></returns>
        public async Task<Result> GetAllTasks()
        {
            var values = new LoginModel() { Username = user.Username, Password = user.Password };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "getalltasks"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();

                    var res = JsonConvert.DeserializeObject<List<TaskModel>>(responseString);

                    return new Result { ResultType = resultType.OK, ReturnObject = res };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// Csoport feladatainak hívácsa, admin csoportvezetők, tagok hívhatják.
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public async Task<Result> GetTasksByGroup(int groupId)
        {
            var values = new IdModel() { Username = user.Username, Password = user.Password, Id = groupId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "GetTasksByGroup"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();

                    var res = JsonConvert.DeserializeObject<List<TaskModel>>(responseString);

                    return new Result { ResultType = resultType.OK, ReturnObject = res };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// Feladat csoporthoz rendelése, csak admin hívhatja.
        /// </summary>
        /// <param name="GroupId"></param>
        /// <param name="TaskId"></param>
        /// <returns></returns>
        public async Task<Result> AssignTaskToGroup(int GroupId, int TaskId)
        {
            var values = new SubjectObjectModel() { Username = user.Username, Password = user.Password, Subject = GroupId, Object = TaskId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "AssignTaskToGroup"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();


                    return new Result { ResultType = resultType.OK };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        ///  Feladat törlése csoportból, csak admin hívhatja.
        /// </summary>
        /// <param name="GroupId"></param>
        /// <param name="TaskId"></param>
        /// <returns></returns>
        public async Task<Result> RemoveTaskFromGroup(int GroupId, int TaskId)
        {
            var values = new SubjectObjectModel() { Username = user.Username, Password = user.Password, Subject = GroupId, Object = TaskId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "RemoveTaskFromGroup"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();


                    return new Result { ResultType = resultType.OK };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// Új részfeladat. admin és csoportvezetők hívhatják.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="parentTaskId"></param>
        /// <returns></returns>
        public async Task<Result> NewSubTask(string title, int parentTaskId)
        {
            var values = new NewSubtaskModel() { Username = user.Username, Password = user.Password, Title = title, ParentId = parentTaskId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "NewSubTask"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();


                    return new Result { ResultType = resultType.OK };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// feladat lekérdezése id alapján. admin, csoportvezetők, csoporttagok hívhatják.
        /// </summary>
        /// <param name="TaskId"></param>
        /// <returns></returns>
        public async Task<Result> GetTask(int TaskId)
        {
            var values = new IdModel() { Username = user.Username, Password = user.Password, Id = TaskId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "NewSubTask"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();

                    var res = JsonConvert.DeserializeObject<TaskModel>(responseString);

                    return new Result { ResultType = resultType.OK, ReturnObject = res };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// összes részfeladat lekérdezése, csak admin hívhatja.
        /// </summary>
        /// <returns></returns>
        public async Task<Result> GetAllSubTasks()
        {
            var values = new LoginModel() { Username = user.Username, Password = user.Password };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "GetAllSubTasks"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();

                    var res = JsonConvert.DeserializeObject<List<SubTaskModel>>(responseString);

                    return new Result { ResultType = resultType.OK, ReturnObject = res };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// részfeladat lekérdezése id alapján. admin, csoportvezetők, csoporttagok hívhatják.
        /// </summary>
        /// <param name="SubTaskId"></param>
        /// <returns></returns>
        public async Task<Result> GetSubTask(int SubTaskId)
        {
            var values = new IdModel() { Username = user.Username, Password = user.Password, Id = SubTaskId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "GetSubTask"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();

                    var res = JsonConvert.DeserializeObject<SubTaskModel>(responseString);

                    return new Result { ResultType = resultType.OK, ReturnObject = res };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// feladat előrehaladásának frissítése, admin és csoportvezetők hívhatják.
        /// </summary>
        /// <param name="TaskId"></param>
        /// <param name="progress"></param>
        /// <returns></returns>
        public async Task<Result> UpdateTaskProgress(int TaskId, int progress)
        {
            var values = new UpdateProgressModel() { Username = user.Username, Password = user.Password, Progress = progress, Id = TaskId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "UpdateTaskProgress"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();

                    return new Result { ResultType = resultType.OK };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// részfeladat előrehaladásának módosítása, admin, csoportvezetők, részfeladathoz hozzárendelt felhasználók hívhatják.
        /// </summary>
        /// <param name="SubTaskId"></param>
        /// <param name="progress"></param>
        /// <returns></returns>
        public async Task<Result> UpdateSubTaskProgress(int SubTaskId, int progress)
        {
            var values = new UpdateProgressModel() { Username = user.Username, Password = user.Password, Progress = progress, Id = SubTaskId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "UpdateSubTaskProgress"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();

                    return new Result { ResultType = resultType.OK };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// felhasználó egyénileg feladatohoz rendelése, admin és csoportvezetők hívhatják.
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="TaskId"></param>
        /// <returns></returns>
        public async Task<Result> AssignUserToTask(int UserId, int TaskId)
        {
            var values = new SubjectObjectModel() { Username = user.Username, Password = user.Password, Subject = UserId, Object = TaskId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "AssignUserToTask"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();

                    return new Result { ResultType = resultType.OK };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// felhasználó eltávolítása egyéni feladatoból, admin és csoportvezetők hívhatják.
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="TaskId"></param>
        /// <returns></returns>
        public async Task<Result> RemoveUserFromTask(int UserId, int TaskId)
        {
            var values = new SubjectObjectModel() { Username = user.Username, Password = user.Password, Subject = UserId, Object = TaskId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "RemoveUserFromTask"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();

                    return new Result { ResultType = resultType.OK };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// Felhasználó hozzárendelése részfeladathoz. admin, csoportvezetők hívhatják.
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="SubTaskId"></param>
        /// <returns></returns>
        public async Task<Result> AssignUserToSubTask(int UserId, int SubTaskId)
        {
            var values = new SubjectObjectModel() { Username = user.Username, Password = user.Password, Subject = UserId, Object = SubTaskId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "AssignUserToSubTask"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();

                    return new Result { ResultType = resultType.OK };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// felhasználó eltávolítása részfeladatból, admin és csoportvezetők hívhatják.
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="SubTaskId"></param>
        /// <returns></returns>
        public async Task<Result> RemoveUserFromSubTask(int UserId, int SubTaskId)
        {
            var values = new SubjectObjectModel() { Username = user.Username, Password = user.Password, Subject = UserId, Object = SubTaskId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "RemoveUserFromSubTask"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();

                    return new Result { ResultType = resultType.OK };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// Felhasználóhoz tartozó egyedi feladatok lehívása, bárki hívhatja.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public async Task<Result> GetTaskByUser(int UserId)
        {
            var values = new IdModel() { Username = user.Username, Password = user.Password, Id = UserId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "GetTaskByUser"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();

                    var res = JsonConvert.DeserializeObject<List<TaskModel>>(responseString);

                    return new Result { ResultType = resultType.OK, ReturnObject = res };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// Felhasználóhoz tartozó részfeladatok lehívása, bárki hívhatja.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public async Task<Result> GetSubTaskByUser(int UserId)
        {
            var values = new IdModel() { Username = user.Username, Password = user.Password, Id = UserId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "GetSubTaskByUser"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();

                    var res = JsonConvert.DeserializeObject<List<SubTaskModel>>(responseString);

                    return new Result { ResultType = resultType.OK, ReturnObject = res };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        /// Adott feladat részfeladatának lehívácsa, admin csoportvezetők, csoporttagok hívhatják.
        /// </summary>
        /// <param name="TaskId"></param>
        /// <returns></returns>
        public async Task<Result> GetSubTaskByTask(int TaskId)
        {
            var values = new IdModel() { Username = user.Username, Password = user.Password, Id = TaskId };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "GetSubTaskByTask"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();

                    var res = JsonConvert.DeserializeObject<List<SubTaskModel>>(responseString);

                    return new Result { ResultType = resultType.OK, ReturnObject = res };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
        /// <summary>
        ///  feladat megjegyzés módosítása, admin és csoportvezetők hívhatják.
        /// </summary>
        /// <param name="TaskId"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        public async Task<Result> UpdateTaskComment(int TaskId, string comment)
        {
            var values = new UpdateCommentModel() { Username = user.Username, Password = user.Password, Id = TaskId, Comment = comment };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.Unicode, "application/json");

            try
            {
                var response = await client.PostAsync(new Uri(Endpoint + "GetSubTaskByTask"), content);

                if (response.IsSuccessStatusCode)
                {

                    var responseString = await response.Content.ReadAsStringAsync();



                    return new Result { ResultType = resultType.OK };
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    return new Result { ResultType = resultType.Error, ReturnObject = null, Message = responseString };
                }
            }
            catch (Exception ex)
            {
                return new Result { ResultType = resultType.Error, ReturnObject = null, Message = ex.Message };
            }
        }
    }
}

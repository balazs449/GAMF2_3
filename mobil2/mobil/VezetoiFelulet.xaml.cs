﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace mobil
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class VezetoiFelulet : Page
    {
        public VezetoiFelulet()
        {
            this.InitializeComponent();

            MainFrame.Navigate(typeof(VezRegisztracio));
            TitleTextBlock.Text = "Regisztráció";
        }

        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            MainSplitView.IsPaneOpen = !MainSplitView.IsPaneOpen;
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (RegisztracioListBoxItem.IsSelected)
            {
                // navigálás
                MainFrame.Navigate(typeof(VezRegisztracio));
                // fejléc beállítása
                TitleTextBlock.Text = "Regisztráció";
            }
            else if (CsapatokLetrehozasaListBoxItem.IsSelected)
            {
                MainFrame.Navigate(typeof(VezCsapatokLetrehozasa));
                TitleTextBlock.Text = "Csapatok lérehozása";
            }
            else if (FeladatokLetrehozasaListBoxItem.IsSelected)
            {
                MainFrame.Navigate(typeof(VezFeladatokLetrehozasa));
                TitleTextBlock.Text = "Feladatok létrehozása";
            }
            else if (EmailListBoxItem.IsSelected)
            {
                MainFrame.Navigate(typeof(VezEmail));
                TitleTextBlock.Text = "Email küldése";
            }
            else if (StatisztikakListBoxItem.IsSelected)
            {
                MainFrame.Navigate(typeof(VezStatisztikak));
                TitleTextBlock.Text = "Statisztikák készítése";
            }
            else if(AttekintesListBoxItem.IsSelected)
            {
                MainFrame.Navigate(typeof(VezAttekintes));
                TitleTextBlock.Text = "Áttekintés";
            }

            MainSplitView.IsPaneOpen = false;
        }

        private void SearchAutoSuggestBox_TextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        {

        }

        private void QuitButton_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Exit();
        }
    }
}

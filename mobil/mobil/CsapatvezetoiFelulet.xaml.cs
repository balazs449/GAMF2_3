﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace mobil
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class CsapatvezetoiFelulet : Page
    {
        public CsapatvezetoiFelulet()
        {
            this.InitializeComponent();

            MainFrame.Navigate(typeof(CsapFeladatok));
            TitleTextBlock.Text = "Feladatok létrehozása";
        }

        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            MainSplitView.IsPaneOpen = !MainSplitView.IsPaneOpen;
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (FeladatokListBoxItem.IsSelected)
            {
                // navigálás
                MainFrame.Navigate(typeof(CsapFeladatok));
                // fejléc beállítása
                TitleTextBlock.Text = "Feladatok létrehozása";
            }
            else if (EgyenekhezRendelesListBoxItem.IsSelected)
            {
                MainFrame.Navigate(typeof(CsapEgyenekhezRendeles));
                TitleTextBlock.Text = "Egyénekhez rendelés";
            }
            else if (MegvalosulasListBoxItem.IsSelected)
            {
                MainFrame.Navigate(typeof(CsapMegvalosulas));
                TitleTextBlock.Text = "Megvalósulás";
            }
            else if (AttekintesListBoxItem2.IsSelected)
            {
                MainFrame.Navigate(typeof(CsapAttekintes));
                TitleTextBlock.Text = "Áttekintés";
            }
                        
            MainSplitView.IsPaneOpen = false;
        }

        private void SearchAutoSuggestBox_TextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        {

        }
    }
}

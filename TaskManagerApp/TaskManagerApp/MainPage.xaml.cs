﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using TaskManagerApp.Helpers;
using TaskManagerApp.Models;
using Windows.UI.Popups;
using TaskManagerApp.Views;
using Microsoft.Toolkit.Uwp;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace TaskManagerApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void loginButton_Click(object sender, RoutedEventArgs e)
        {
            Windows.Storage.ApplicationDataContainer localSettings =  Windows.Storage.ApplicationData.Current.LocalSettings;
            var api = new TaskManagerApp.Helpers.ApiAccess(usernameTextBox.Text, passwordPasswordBox.Password, "https://huszaralex.a2hosted.com/api/v1/");
            var res = await api.Login();
            if (res.ResultType == ApiAccess.resultType.OK)
            {
               localSettings.Values["username"]= usernameTextBox.Text;
               localSettings.Values["password"] = passwordPasswordBox.Password;


                var storage = new LocalObjectStorageHelper();
                storage.Save("api", api);
                if ((bool)res.ReturnObject) { this.Frame.Navigate(typeof(AdminMenu),api); }
                else { this.Frame.Navigate(typeof(UserMenu),api); }
            }
            else
            {
                var dialog = new MessageDialog(res.Message, "Error");
                await dialog.ShowAsync();
            }
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            var api = new ApiAccess("", "", "https://huszaralex.a2hosted.com/api/v1/");
            var test = await api.Test();
            if (test.ResultType!=ApiAccess.resultType.OK)
            {
                MessageDialog diaog = new MessageDialog("Nem sikerült kapcsolatot létesíteni a szerverrel: "+test.Message);
                await diaog.ShowAsync();
            }

        }

        private void StackPanel_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                loginButton_Click(null, null);
            }
        }
    }
}

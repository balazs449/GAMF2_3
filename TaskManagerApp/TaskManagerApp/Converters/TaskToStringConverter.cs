﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskManagerApp.Models;
using Windows.UI.Xaml.Data;

namespace TaskManagerApp.Converters
{
    class TaskToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            IEnumerable<TaskModel> users = (IEnumerable<TaskModel>)value;
            IEnumerable<string> Names = users.Select(a => a.Title);


            return string.Join(", ", Names);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}

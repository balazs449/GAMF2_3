﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;
using TaskManagerApp.Models;

namespace TaskManagerApp.Converters
{
    class UsersToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            IEnumerable<PersonModel> users = (IEnumerable<PersonModel>)value;
            IEnumerable<string> Names = users.Select(a => a.UserName);


            return string.Join(", ", Names);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}

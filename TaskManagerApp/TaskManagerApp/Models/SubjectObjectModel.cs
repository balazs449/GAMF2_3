﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskManagerApp.Models
{
    [System.Runtime.Serialization.DataContract]
    public class SubjectObjectModel
    {
        [System.Runtime.Serialization.DataMember]
        public string Username { get; set; }
        [System.Runtime.Serialization.DataMember]
        public string Password { get; set; }
        [System.Runtime.Serialization.DataMember]
        public int Subject { get; set; }
        [System.Runtime.Serialization.DataMember]
        public int Object { get; set; }
    }
}

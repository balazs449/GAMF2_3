﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskManagerApp.Models
{
    public class UpdateCommentModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Comment { get; set; }
        public int Id { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using TaskManagerApp.Models;
using System.Collections.ObjectModel;
using TaskManagerApp.Helpers;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace TaskManagerApp.Views
{

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PeoplePage : Page
    {
        ApiAccess api;
        public ObservableCollection<PersonModel> PersonList { get; set; }
        public PeoplePage()
        {
            this.InitializeComponent();
        }

        private async void ListView_Loaded(object sender, RoutedEventArgs e)
        {

        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            api = e.Parameter as ApiAccess;
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            var res = await api.GetAllUsers();
            if (res.ResultType == ApiAccess.resultType.OK)
            {

                List<PersonModel> persons = (List<PersonModel>)res.ReturnObject;
                PersonList = new ObservableCollection<PersonModel>();


                persons.ForEach(a => PersonList.Add(a));
                personsListView.ItemsSource = PersonList;
                //var dialog = new Windows.UI.Popups.MessageDialog(Newtonsoft.Json.JsonConvert.SerializeObject(PersonList));
                //await dialog.ShowAsync();


            }
            else
            {
                var dialog = new Windows.UI.Popups.MessageDialog(res.Message);
                await dialog.ShowAsync();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TaskManagerApp.Helpers;
using TaskManagerApp.Models;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace TaskManagerApp.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class RemoveTaskFromGroupPage : Page
    {
        public RemoveTaskFromGroupPage()
        {
            this.InitializeComponent();
        }

        ApiAccess api;
        GroupModel group;
        List<TaskModel> tasks;
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            List<Object> a = (List<object>)e.Parameter;
            api = a[0] as ApiAccess;
            group = a[1] as GroupModel;
            //IEnumerable<int> ids = group.Tasks.Select(usr => usr.Id);
            var res = await api.GetTasksByGroup(group.Id);

            tasks = (List<TaskModel>)res.ReturnObject;


            personsListView.ItemsSource = tasks;

        }

        private async void saveButton_Click(object sender, RoutedEventArgs e)
        {
            bool ok = true;
            for (int i = 0; i < personsListView.SelectedItems.Count; i++)
            {
                TaskModel task = (TaskModel)personsListView.SelectedItems[i];
                var res = await api.RemoveTaskFromGroup(GroupId: group.Id, TaskId: task.Id);
                if (res.ResultType == ApiAccess.resultType.OK)
                {



                }
                else
                {
                    ok = false;
                    var dialog = new MessageDialog(res.Message);
                    await dialog.ShowAsync();
                }


            }
            if (ok)
            {
                var dialog = new MessageDialog("ok");
                await dialog.ShowAsync();
                this.Frame.GoBack();
            }

        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }
    }
}

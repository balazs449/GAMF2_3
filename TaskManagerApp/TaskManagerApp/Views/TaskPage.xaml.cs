﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TaskManagerApp.Helpers;
using TaskManagerApp.Models;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace TaskManagerApp.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class TaskPage : Page
    {
        public TaskPage()
        {
            this.InitializeComponent();
        }
        ApiAccess api;
        TaskModel task;

        private async void taskReportProgress_Click(object sender, RoutedEventArgs e)
        {
            int prog;

            try
            {
                prog = int.Parse(taskNewPercent.Text);

                var res = await api.UpdateTaskProgress(TaskId: task.Id,progress:prog);
                if (res.ResultType == ApiAccess.resultType.OK)
                {

                    RefreshButton_Click(null, null);
                }
                else
                {
                    var dialog = new MessageDialog(res.Message);
                    await dialog.ShowAsync();
                }
            }
            catch(Exception ex)
            {
                var dialog = new MessageDialog(ex.Message);
                await dialog.ShowAsync();
            }
        }

        private async void AddSubTask_Click(object sender, RoutedEventArgs e)
        {
            var dialog1 = new NewSubTaskContentDialogue() { PrimaryButtonText="Ok",SecondaryButtonText="Cancel"};
            var result = await dialog1.ShowAsync();
            if (result == ContentDialogResult.Primary)
            {
                var text = dialog1.Text;

                var res = await api.NewSubTask(parentTaskId: task.Id, title: text);
                if (res.ResultType == ApiAccess.resultType.OK)
                {

                    RefreshButton_Click(null, null);
                }
                else
                {
                    var dialog = new MessageDialog(res.Message);
                    await dialog.ShowAsync();
                }
            }
        }

        private void RemoveSubTask_Click(object sender, RoutedEventArgs e)
        {

        }
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            var userConver = new Converters.UsersToStringConverter();
            var taskConver = new Converters.TaskToStringConverter();
            List<Object> a = (List<object>)e.Parameter;
            api = a[0] as ApiAccess;

            task = a[1] as TaskModel;
            int id = task.Id;

            var res = await api.GetTask(id);
            if (res.ResultType == ApiAccess.resultType.OK)
            {

                task = (TaskModel)res.ReturnObject;


            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }

            taskId.Text = task.Id.ToString();
            taskTitle.Text = task.Title;
            taskDeadline.Date = task.Deadline.Date;
            taskStartDate.Date = task.StartDate.Date;
            taskProgressPercent.Text = task.Progress + "%";
            taskPrgoressBar.Value = task.Progress;
            taskComment.Text = task.Comment;
            TaskViewModel.ItemsSource = task.SubTask;


            //var dialog2 = new MessageDialog(Newtonsoft.Json.JsonConvert.SerializeObject(task.SubTask));
            //await dialog2.ShowAsync();

        }







        private void BackButton_Click_1(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }

        private async void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            var userConver = new Converters.UsersToStringConverter();
            var taskConver = new Converters.TaskToStringConverter();
            int id = task.Id;

            var res = await api.GetTask(id);
            if (res.ResultType == ApiAccess.resultType.OK)
            {

                task = (TaskModel)res.ReturnObject;


            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }

            taskId.Text = task.Id.ToString();
            taskTitle.Text = task.Title;
            taskDeadline.Date = task.Deadline.Date;
            taskStartDate.Date = task.StartDate.Date;
            taskProgressPercent.Text = task.Progress + "%";
            taskPrgoressBar.Value = task.Progress;
            taskComment.Text = task.Comment;

            TaskViewModel.ItemsSource = task.SubTask;

        }

        private async void taskUpdaetComment_Click(object sender, RoutedEventArgs e)
        {

            var res = await api.UpdateTaskComment(TaskId: task.Id, comment: NewComment.Text);
            if (res.ResultType == ApiAccess.resultType.OK)
            {

                RefreshButton_Click(null, null);
            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            Button b = (Button)e.OriginalSource;
            var res = await api.DeleteSubTask((int)b.Tag);
            if (res.ResultType == ApiAccess.resultType.OK)
            {

                RefreshButton_Click(null, null);
            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Button b = (Button)e.OriginalSource;


            var dialog1 = new NewSubTaskContentDialogue() { PrimaryButtonText = "Ok", SecondaryButtonText = "Cancel" ,Title="Progress"};
            var result = await dialog1.ShowAsync();
            if (result == ContentDialogResult.Primary)
            {


                try
                {
                    int prog = int.Parse(dialog1.Text);

                    var res = await api.UpdateSubTaskProgress(SubTaskId: (int)b.Tag,progress:prog);


                    if (res.ResultType == ApiAccess.resultType.OK)
                    {

                        RefreshButton_Click(null, null);
                    }
                    else
                    {
                        var dialog = new MessageDialog(res.Message);
                        await dialog.ShowAsync();
                    }
                } catch (Exception ex) {
                    var dialog = new MessageDialog(ex.Message);
                    await dialog.ShowAsync();
                }
            }


        }

        private void OwnSubTasks_Click(object sender, RoutedEventArgs e)
        {
            List<object> a = new List<object> { api, task };
            this.Frame.Navigate(typeof(OwnSubTasksPage), a);
        }

        private async void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Button b = (Button)e.OriginalSource;
            var con = new Converters.UsersToStringConverter();

            var res = await api.GetSubTaskPersons(SubTaskId: (int)b.Tag);


            if (res.ResultType == ApiAccess.resultType.OK)
            {
                List<PersonModel> persons = (List<PersonModel>)res.ReturnObject;
                string personstrnig = "Persons: " + (string)con.Convert(persons,null,null,null);
                var dialog = new MessageDialog(personstrnig,"ok") { Title="Info"};
                await dialog.ShowAsync();
            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            Button b = (Button)e.OriginalSource;
            int id = (int)b.Tag;
            string subtaskid = id.ToString();
            List<object> a = new List<object> { api, task,  subtaskid};
            this.Frame.Navigate(typeof(AssignUserToSubTaskPage), a);
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            Button b = (Button)e.OriginalSource;
            int id = (int)b.Tag;
            string subtaskid = id.ToString();
            List<object> a = new List<object> { api, task, subtaskid };
            this.Frame.Navigate(typeof(RemoveSubTaskFromUserPage), a);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using TaskManagerApp.Helpers;
using TaskManagerApp.Models;
using Windows.UI.Popups;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace TaskManagerApp.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class GroupPage : Page
    {
        ApiAccess api;
        GroupModel group;
        public GroupPage()
        {
            this.InitializeComponent();
        }
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            var userConver = new Converters.UsersToStringConverter();
            var taskConver = new Converters.TaskToStringConverter();
            List<Object> a = (List<object>)e.Parameter;
            api = a[0] as ApiAccess;

            group = a[1] as GroupModel;
            int id = group.Id;

            var res = await api.GetGroupsById(id);
            if (res.ResultType == ApiAccess.resultType.OK)
            {

                group = (GroupModel)res.ReturnObject;


            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }

            groupId.Text = group.Id.ToString();
            groupName.Text = group.Name;
            groupLeaders.Text = (string)userConver.Convert(group.Leaders,null,null,null);
            groupMembers.Text = (string)userConver.Convert(group.Members, null, null, null);
            TaskViewModel.ItemsSource = group.Tasks;

            var log = await api.Login();
            bool admin = (bool)log.ReturnObject;
            if (admin)
            {
                NewLeader.IsEnabled = true;
                NewMember.IsEnabled = true;
                AddTask.IsEnabled = true;
                RemoveLeader.IsEnabled = true;
                RemoveTask.IsEnabled = true;
                RemoveMember.IsEnabled = true;
            }
            else {
                log = await api.GetLeadGroups();
                List<GroupModel> groups = (List<GroupModel>)log.ReturnObject;
                if (groups.FirstOrDefault(gr=>gr.Id==group.Id)!=null) {

                    NewMember.IsEnabled = true;

                    RemoveMember.IsEnabled = true;
                }
            }


        }

        private void NewMember_Click(object sender, RoutedEventArgs e)
        {
            List<object> a = new List<object> { api, group };
            this.Frame.Navigate(typeof(AddMemberToGroupPage), a);
        }

        private void RemoveMember_Click(object sender, RoutedEventArgs e)
        {
            List<object> a = new List<object> { api, group };
            this.Frame.Navigate(typeof(RemoveMemberFromGroupPage), a);
        }

        private void NewLeader_Click(object sender, RoutedEventArgs e)
        {
            
            List<object> a = new List<object> { api, group };
            this.Frame.Navigate(typeof(AddLeaderToGroupPage), a);
        }

        private void RemoveLeader_Click(object sender, RoutedEventArgs e)
        {
            List<object> a = new List<object> { api, group };
            this.Frame.Navigate(typeof(RemoveLeaderFromGorpuPage), a);
        }

        private void BackButton_Click_1(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }

        private async void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            var userConver = new Converters.UsersToStringConverter();
            var taskConver = new Converters.TaskToStringConverter();
            int id = group.Id;

            var res = await api.GetGroupsById(id);
            if (res.ResultType == ApiAccess.resultType.OK)
            {

                group = (GroupModel)res.ReturnObject;


            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }

            groupId.Text = group.Id.ToString();
            groupName.Text = group.Name;
            groupLeaders.Text = (string)userConver.Convert(group.Leaders, null, null, null);
            groupMembers.Text = (string)userConver.Convert(group.Members, null, null, null);
            TaskViewModel.ItemsSource = group.Tasks;

            var log = await api.Login();
            bool admin = (bool)log.ReturnObject;
            if (admin)
            {
                NewLeader.IsEnabled = true;
                NewMember.IsEnabled = true;
                AddTask.IsEnabled = true;
                RemoveLeader.IsEnabled = true;
                RemoveTask.IsEnabled = true;
                RemoveMember.IsEnabled = true;
            }
            else
            {
                log = await api.GetLeadGroups();
                List<GroupModel> groups = (List<GroupModel>)log.ReturnObject;
                if (groups.FirstOrDefault(gr => gr.Id == group.Id) != null)
                {

                    NewMember.IsEnabled = true;

                    RemoveMember.IsEnabled = true;
                }
            }

        }

        private void AddTask_Click(object sender, RoutedEventArgs e)
        {
            List<object> a = new List<object> { api, group };
            this.Frame.Navigate(typeof(AddTaskToGroupPage), a);
        }

        private void RemoveTask_Click(object sender, RoutedEventArgs e)
        {
            List<object> a = new List<object> { api, group };
            this.Frame.Navigate(typeof(RemoveTaskFromGroupPage), a);
        }

        private void TaskViewModel_ItemClick(object sender, ItemClickEventArgs e)
        {
            TaskModel task = (TaskModel)e.ClickedItem;
            List<object> a = new List<object> { api, task };
            this.Frame.Navigate(typeof(TaskPage), a);
        }
    }
}

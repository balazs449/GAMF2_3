﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TaskManagerApp.Helpers;
using TaskManagerApp.Models;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace TaskManagerApp.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class TasksPage : Page
    {

        public TasksPage()
        {
            this.InitializeComponent();
        }
        ApiAccess api;

        List<TaskModel> tasks;
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            api = e.Parameter as ApiAccess;

            var res = await api.GetAllTasks();

            tasks = (List<TaskModel>)res.ReturnObject;


            personsListView.ItemsSource = tasks;

        }

        private void personsListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            TaskModel task = (TaskModel)e.ClickedItem;
            List<object> a = new List<object> { api, task };
            this.Frame.Navigate(typeof(TaskPage), a);
        }
    }
}

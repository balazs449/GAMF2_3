﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TaskManagerApp.Helpers;
using TaskManagerApp.Models;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace TaskManagerApp.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class OwnGroups : Page
    {
        ApiAccess api;
        ObservableCollection<GroupModel> groupList;
        public OwnGroups()
        {
            this.InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            api = e.Parameter as ApiAccess;

        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {


            var res = await api.GetOwnGroups();
            if (res.ResultType == ApiAccess.resultType.OK)
            {

                List<GroupModel> groups = (List<GroupModel>)res.ReturnObject;
                groupList = new ObservableCollection<GroupModel>();


                groups.ForEach(a => groupList.Add(a));
                personsListView.ItemsSource = groupList;
                //var dialog = new Windows.UI.Popups.MessageDialog(Newtonsoft.Json.JsonConvert.SerializeObject(PersonList));
                //await dialog.ShowAsync();


            }
            else
            {
                var dialog = new Windows.UI.Popups.MessageDialog(res.Message,res.ResultType.ToString());
                await dialog.ShowAsync();
            }
        }
        private void personsListView_ItemClick(object sender, ItemClickEventArgs e)
        {

            GroupModel group = (GroupModel)e.ClickedItem;
            List<object> a = new List<object> { api, group };
            this.Frame.Navigate(typeof(GroupPage), a);
        }
    }
}

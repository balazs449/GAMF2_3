﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TaskManagerApp.Helpers;
using TaskManagerApp.Models;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace TaskManagerApp.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class OwnSubTasksPage : Page
    {
        public OwnSubTasksPage()
        {
            this.InitializeComponent();
        }
        ApiAccess api;
        TaskModel task;
        List<SubTaskModel> subtasks;
        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Button b = (Button)e.OriginalSource;


            var dialog1 = new NewSubTaskContentDialogue() { PrimaryButtonText = "Ok", SecondaryButtonText = "Cancel", Title = "Progress" };
            var result = await dialog1.ShowAsync();
            if (result == ContentDialogResult.Primary)
            {


                try
                {
                    int prog = int.Parse(dialog1.Text);

                    var res = await api.UpdateSubTaskProgress(SubTaskId: (int)b.Tag, progress: prog);


                    if (res.ResultType == ApiAccess.resultType.OK)
                    {

                        Refresh();
                    }
                    else
                    {
                        var dialog = new MessageDialog(res.Message);
                        await dialog.ShowAsync();
                    }
                }
                catch (Exception ex)
                {
                    var dialog = new MessageDialog(ex.Message);
                    await dialog.ShowAsync();
                }
            }


        }
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            var userConver = new Converters.UsersToStringConverter();
            var taskConver = new Converters.TaskToStringConverter();
            List<Object> a = (List<object>)e.Parameter;
            api = a[0] as ApiAccess;

            task = a[1] as TaskModel;
            int id = task.Id;


            var res = await api.GetOwnSubTasksByTask(id);
            if (res.ResultType == ApiAccess.resultType.OK)
            {

                subtasks = (List<SubTaskModel>)res.ReturnObject;


            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }
            TaskViewModel.ItemsSource = subtasks;


            //var dialog2 = new MessageDialog(Newtonsoft.Json.JsonConvert.SerializeObject(task.SubTask));
            //await dialog2.ShowAsync();

        }
        private async void Refresh()
        {
            var userConver = new Converters.UsersToStringConverter();
            var taskConver = new Converters.TaskToStringConverter();
            int id = task.Id;

            var res = await api.GetTask(id);
            if (res.ResultType == ApiAccess.resultType.OK)
            {

                task = (TaskModel)res.ReturnObject;


            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }

            TaskViewModel.ItemsSource = task.SubTask;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }
    }
}

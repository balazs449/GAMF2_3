﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TaskManagerApp.Helpers;
using TaskManagerApp.Models;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace TaskManagerApp.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class RemoveSubTaskFromUserPage : Page
    {
        public RemoveSubTaskFromUserPage()
        {
            this.InitializeComponent();
        }
        ApiAccess api;
        TaskModel task;
        GroupModel group;
        int subtaskid;
        List<PersonModel> users;
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            List<Object> a = (List<object>)e.Parameter;
            api = a[0] as ApiAccess;

            task = a[1] as TaskModel;
            string tag = a[2] as string;
            subtaskid = int.Parse(tag);

            var res = await api.GetGroupByTask(task.Id);
            if (res.ResultType == ApiAccess.resultType.OK)
            {
                group = (GroupModel)res.ReturnObject;


                var res2 = await api.GetSubTaskPersons(subtaskid);
                users = (List<PersonModel>)res2.ReturnObject;
                personsListView.ItemsSource = users;
            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }
        }

        private async void saveButton_Click(object sender, RoutedEventArgs e)
        {
            bool ok = true;
            for (int i = 0; i < personsListView.SelectedItems.Count; i++)
            {
                PersonModel user = (PersonModel)personsListView.SelectedItems[i];
                var res = await api.RemovePersonFromSubtask(SubTaskId: subtaskid, UserId: user.Id);
                if (res.ResultType == ApiAccess.resultType.OK)
                {



                }
                else
                {
                    ok = false;
                    var dialog = new MessageDialog(res.Message);
                    await dialog.ShowAsync();
                }


            }
            if (ok)
            {
                var dialog = new MessageDialog("ok");
                await dialog.ShowAsync();
                this.Frame.GoBack();
            }

        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }
    }
}

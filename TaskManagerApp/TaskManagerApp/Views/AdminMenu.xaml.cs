﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using TaskManagerApp.Helpers;
using TaskManagerApp.Models;
using Windows.UI.Popups;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace TaskManagerApp.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AdminMenu : Page
    {
        private ApiAccess api;
        public AdminMenu()
        {
            this.InitializeComponent();
        }

        private void navigateBackButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }

        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            MainSliptView.IsPaneOpen = !MainSliptView.IsPaneOpen;
        }
        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (BackListBoxItem.IsSelected)
            {
                this.Frame.GoBack();
            }
            else if (GroupsListBoxItem.IsSelected)
            {
                // navigálás
                MainFrame.Navigate(typeof(GroupsPage),api);
                // fejléc beállítása
                TitleTextBlock.Text = "Groups";
            }
            else if (NewGroupListBoxItem.IsSelected)
            {
                // navigálás
                MainFrame.Navigate(typeof(NewGroupPage), api);
                // fejléc beállítása
                TitleTextBlock.Text = "New group";
            }
            else if (TasksListBoxItem.IsSelected)
            {
                // navigálás
                MainFrame.Navigate(typeof(TasksPage), api);
                // fejléc beállítása
                TitleTextBlock.Text = "Tasks";
            }
            else if (NewTaskListBoxItem.IsSelected)
            {
                // navigálás
                MainFrame.Navigate(typeof(NewTaskPage), api);
                // fejléc beállítása
                TitleTextBlock.Text = "New task";
            }
            else if (HomeListBoxItem.IsSelected)
            {
                // navigálás
                MainFrame.Navigate(typeof(HomePage),api);
                // fejléc beállítása
                TitleTextBlock.Text = "Home";
            }
            else if (NewUserListBoxItem.IsSelected)
            {
                MainFrame.Navigate(typeof(NewUserPage),api);
                TitleTextBlock.Text = "Add new user";
            }
            else if (OwnGroupsListBoxItem.IsSelected)
            {
                MainFrame.Navigate(typeof(OwnGroups),api);
                TitleTextBlock.Text = "Own groups";
            }
            else if (PeopleListBoxItem.IsSelected)
            {
                MainFrame.Navigate(typeof(PeoplePage),api);
                TitleTextBlock.Text = "Users";
            }
            else if (LeadGroupsListBoxItem.IsSelected)
            {
                MainFrame.Navigate(typeof(LeadGroupsPage),api);
                TitleTextBlock.Text = "Lead groups";
            }

            //MainSliptView.IsPaneOpen = false;
        }

               

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            api = e.Parameter as ApiAccess;
        }

        private void TextBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }

        private void TextBlock_SelectionChanged_1(object sender, RoutedEventArgs e)
        {

        }
    }
}

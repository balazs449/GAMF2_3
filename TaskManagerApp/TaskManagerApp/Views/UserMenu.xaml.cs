﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TaskManagerApp.Helpers;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace TaskManagerApp.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class UserMenu : Page
    {
        ApiAccess api;
        public UserMenu()
        {
            this.InitializeComponent();
        }

        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            MainSliptView.IsPaneOpen = !MainSliptView.IsPaneOpen;
        }
        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (BackListBoxItem.IsSelected)
            {
                this.Frame.GoBack();
            }



            else if (HomeListBoxItem.IsSelected)
            {
                // navigálás
                MainFrame.Navigate(typeof(HomePage), api);
                // fejléc beállítása
                TitleTextBlock.Text = "Home";
            }

            else if (OwnGroupsListBoxItem.IsSelected)
            {
                MainFrame.Navigate(typeof(OwnGroups), api);
                TitleTextBlock.Text = "Own groups";
            }
            else if (PeopleListBoxItem.IsSelected)
            {
                MainFrame.Navigate(typeof(PeoplePage), api);
                TitleTextBlock.Text = "Users";
            }
            else if (LeadGroupsListBoxItem.IsSelected)
            {
                MainFrame.Navigate(typeof(LeadGroupsPage), api);
                TitleTextBlock.Text = "Lead groups";
            }

            //MainSliptView.IsPaneOpen = false;
        }


        private void navigateBackButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            api = e.Parameter as ApiAccess;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace App5
{

    class NewUserModel
    {
        [Required]

        public string Username { get; set; }
        [Required]

        public string Password { get; set; }
        [Required]

        public string NewUserUsername { get; set; }
        [Required]

        public string NewUserPassword { get; set; }
        [Required]

        public string NewUserName { get; set; }
        [Required]

        public string NewUserEmail { get; set; }
        [Required]

        public bool NewUserAdmin { get; set; }
    }
}

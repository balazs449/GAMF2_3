﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Web.Http;
using Newtonsoft.Json;
using TaskManagerApp.Helpers;
using TaskManagerApp.Models;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace App5
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            api = new TaskManagerApp.Helpers.ApiAccess("admin", "admin", "https://huszaralex.a2hosted.com/api/v1/");
        }
        TaskManagerApp.Helpers.ApiAccess api;

        private async void LoginTest(object sender, RoutedEventArgs e)
        {

            //HttpClient client = new HttpClient();
            //    var values = new Dictionary<string, string>
            //    {
            //        { "Username", "admin" },
            //        { "Password", "admin" }
            //    };

            //    var content = new FormUrlEncodedContent(values);

            //    var response = await client.PostAsync("https://huszaralex.a2hosted.com/api/v1/login", content);

            //    var responseString = await response.Content.ReadAsStringAsync();

            //    //RespTextBox.Text = responseString;
            //    var dialog = new MessageDialog(responseString);
            //    await dialog.ShowAsync();

            api = new TaskManagerApp.Helpers.ApiAccess(logintestUsername.Text, logintestPassword.Text, "https://huszaralex.a2hosted.com/api/v1/");

            var res = await api.Login();
            if (res.ResultType == ApiAccess.resultType.OK)
            {

                MessageDialog dialog;
                if ((bool)res.ReturnObject) { dialog = new MessageDialog("admin"); }
                else { dialog = new MessageDialog("user"); }
                await dialog.ShowAsync();
            }
            else {
                var dialog = new MessageDialog(res.Message,"Error");
                await dialog.ShowAsync();
            }
        }
        private async void NewUserTest(object sender, RoutedEventArgs e)
        {
            var res = await api.NewUser((bool)newuserAdmin.IsChecked,newuserUsername.Text, newuserPassword.Text, newUserEmail.Text,newuserName.Text);
            if (res.ResultType == ApiAccess.resultType.OK)
            {
                var dialog = new MessageDialog("ok");
                await dialog.ShowAsync();
               
            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }
        }

        private async void GetAllUsersTest(object sender, RoutedEventArgs e)
        {


            var res = await api.GetAllUsers();
            if (res.ResultType == ApiAccess.resultType.OK)
            {

                List<PersonModel> persons = (List<PersonModel>)res.ReturnObject;

                    var dialog = new MessageDialog(JsonConvert.SerializeObject(persons));
                    await dialog.ShowAsync();


            }
            else {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }

        }

        private async void GetAllAdminsTest(object sender, RoutedEventArgs e)
        {
            var res = await api.GetAllAdmins();
            if (res.ResultType == ApiAccess.resultType.OK)
            {

                List<PersonModel> persons = (List<PersonModel>)res.ReturnObject;

                var dialog = new MessageDialog(JsonConvert.SerializeObject(persons));
                await dialog.ShowAsync();


            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }
        }

        private async void GetAllLeadersTest(object sender, RoutedEventArgs e)
        {
            var res = await api.GetAllLeaders();
            if (res.ResultType == ApiAccess.resultType.OK)
            {

                List<PersonModel> persons = (List<PersonModel>)res.ReturnObject;

                var dialog = new MessageDialog(JsonConvert.SerializeObject(persons));
                await dialog.ShowAsync();


            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }
        }

        private async void GetAllSkillsTest(object sender, RoutedEventArgs e)
        {
            var res = await api.GetAllSkills();
            if (res.ResultType == ApiAccess.resultType.OK)
            {

                List<SkillModel> persons = (List<SkillModel>)res.ReturnObject;

                var dialog = new MessageDialog(JsonConvert.SerializeObject(persons));
                await dialog.ShowAsync();


            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }
        }

        private async void GetOwnSkillsTest(object sender, RoutedEventArgs e)
        {
            var res = await api.GetOwnSkills();
            if (res.ResultType == ApiAccess.resultType.OK)
            {

                List<SkillModel> persons = (List<SkillModel>)res.ReturnObject;

                var dialog = new MessageDialog(JsonConvert.SerializeObject(persons));
                await dialog.ShowAsync();


            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }
        }
        private async void AddSkillToUserTest(object sender, RoutedEventArgs e)
        {
            var res = await api.AddSkillToUser(int.Parse(addskillobj.Text),int.Parse(addskillsub.Text));
            if (res.ResultType == ApiAccess.resultType.OK)
            {

                var dialog = new MessageDialog("ok");
                await dialog.ShowAsync();


            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }
        }
        private async void RemoveSkillFromUserTest(object sender, RoutedEventArgs e)
        {
            var res = await api.RemoveSkillFromUser(int.Parse(remskillobj.Text), int.Parse(remskillsub.Text));
            if (res.ResultType == ApiAccess.resultType.OK)
            {

                var dialog = new MessageDialog("ok");
                await dialog.ShowAsync();


            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }
        }

        private async void AddUserToGroupTest_Click(object sender, RoutedEventArgs e)
        {
            var res = await api.AddUserToGroup(int.Parse(addusertogroupuserid.Text), int.Parse(addusertogroupgroupid.Text));
            if (res.ResultType == ApiAccess.resultType.OK)
            {

                var dialog = new MessageDialog("ok");
                await dialog.ShowAsync();


            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }
        }

        private async void RemoveUserFromGroupTest_Click(object sender, RoutedEventArgs e)
        {
            var res = await api.RemoveUserFromGroup(int.Parse(remuserfromgroupuserid.Text), int.Parse(remuserfromgroupgroupid.Text));
            if (res.ResultType == ApiAccess.resultType.OK)
            {

                var dialog = new MessageDialog("ok");
                await dialog.ShowAsync();


            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }
        }

        private async void AddLeaderToGroupTest_Click(object sender, RoutedEventArgs e)
        {
            var res = await api.AddLeaderToGroup(int.Parse(addleadertogroupuserid.Text), int.Parse(addleadertogroupgroupid.Text));
            if (res.ResultType == ApiAccess.resultType.OK)
            {
                var dialog = new MessageDialog("ok");
                await dialog.ShowAsync();


            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }
        }

        private async void RemoveLeaderFromGroupTest_Click(object sender, RoutedEventArgs e)
        {
            var res = await api.RemoveLeaderFromGroup(int.Parse(removeleaderfromgroupuserid.Text), int.Parse(removeleaderfromgroupgroupid.Text));
            if (res.ResultType == ApiAccess.resultType.OK)
            {



                var dialog = new MessageDialog("ok");
                await dialog.ShowAsync();


            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }
        }

        private async void NewGroupTest_Click(object sender, RoutedEventArgs e)
        {
            var res = await api.NewGroup(newgrouptitle.Text);
            if (res.ResultType == ApiAccess.resultType.OK)
            {



                var dialog = new MessageDialog("ok");
                await dialog.ShowAsync();


            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }
        }

        private async void GetAllGroupsTest_Click(object sender, RoutedEventArgs e)
        {
            var res = await api.GetAllGroups();
            if (res.ResultType == ApiAccess.resultType.OK)
            {

                List<GroupModel> persons = (List<GroupModel>)res.ReturnObject;

                var dialog = new MessageDialog(JsonConvert.SerializeObject(persons));
                await dialog.ShowAsync();


            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }
        }

        private async void GetOwnGroupsTest_Click(object sender, RoutedEventArgs e)
        {
            var res = await api.GetOwnGroups();
            if (res.ResultType == ApiAccess.resultType.OK)
            {

                List<GroupModel> persons = (List<GroupModel>)res.ReturnObject;

                var dialog = new MessageDialog(JsonConvert.SerializeObject(persons));
                await dialog.ShowAsync();


            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }
        }

        private async void GetLeadGroupsTest_Click(object sender, RoutedEventArgs e)
        {
            var res = await api.GetLeadGroups();
            if (res.ResultType == ApiAccess.resultType.OK)
            {

                List<GroupModel> persons = (List<GroupModel>)res.ReturnObject;

                var dialog = new MessageDialog(JsonConvert.SerializeObject(persons));
                await dialog.ShowAsync();


            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }
        }

        private async void GetUserByIdTest_Click(object sender, RoutedEventArgs e)
        {
            var res = await api.GetUserById(int.Parse(getuserbyid.Text));
            if (res.ResultType == ApiAccess.resultType.OK)
            {

                PersonModel persons = (PersonModel)res.ReturnObject;

                var dialog = new MessageDialog(JsonConvert.SerializeObject(persons));
                await dialog.ShowAsync();


            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }

        }

        private async void DeleteGroupTest_Click(object sender, RoutedEventArgs e)
        {
            var res = await api.DeleteGroup(int.Parse(deletegroup.Text));
            if (res.ResultType == ApiAccess.resultType.OK)
            {

               

                var dialog = new MessageDialog("ok");
                await dialog.ShowAsync();


            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }
        }

        private async void NewSkillTest_Click(object sender, RoutedEventArgs e)
        {
            var res = await api.NewSkill(newskillname.Text);
            if (res.ResultType == ApiAccess.resultType.OK)
            {



                var dialog = new MessageDialog("ok");
                await dialog.ShowAsync();


            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }
        }

        private async void NewTaskTest_Click(object sender, RoutedEventArgs e)
        {
            var res = await api.NewTask(title:newtasktitle.Text,start:newtaskstartdate.Date.Date,deadline:newtaskdeadline.Date.Date,comment:newtaskcomment.Text);
            if (res.ResultType == ApiAccess.resultType.OK)
            {
                var dialog = new MessageDialog("ok");
                await dialog.ShowAsync();
            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }
        }

        private async void GetAllTasksTest_Click(object sender, RoutedEventArgs e)
        {
            var res = await api.GetAllTasks();
            if (res.ResultType == ApiAccess.resultType.OK)
            {
                List<TaskModel> persons = (List<TaskModel>)res.ReturnObject;

                var dialog = new MessageDialog(JsonConvert.SerializeObject(persons));
                await dialog.ShowAsync();
            }
            else
            {
                var dialog = new MessageDialog(res.Message);
                await dialog.ShowAsync();
            }
        }
    }
}
